#include "GUI/loginwindow.h"
#include "SQL/datenbankerstellung.h"
#include <QApplication>

/**********************
 *          DEBUG              *
 **********************/
#include <QDebug>
#include <SQL/ArbeitenContainer.h>
#include <SQL/Arbeit.h>
#include <Data/Dozent.h>
#include <QSql>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <memory>
#include "Data/Administrator.h"
#include "Data/datenfacade.h"

int main(int argc, char *argv[])
{
    DatenbankErstellung _db = DatenbankErstellung();
    Datenfacade _df = Datenfacade::getfacade();
    QList<QString> _testlist;
    _testlist.append("Nils Birkner");
    _testlist.append("Manuel Mayer");
    _testlist.append("Julius Röhrich");
    _testlist.append("Der Typ aus der vorletzten Reihe");
    if(_testlist.size() == 0)
    {
        return 1;
    }
    //std::unique_ptr<Administrator> _testadmin(new Administrator("nils@birkner.de", "12345"));
    std::shared_ptr<Dozent> _testdozent(new Dozent("Nils", "Birkner", "nils@birkner.de", "abcde"));
    std::shared_ptr<Arbeit> _testarbeit(new Arbeit("Sonstiges", "Die Theorie des Nichts", "HalloWelt", "Gemacht", "IT", "Ich bin zu faul!", _testlist, _testdozent.get()));
    //std::unique_ptr<ArbeitenContainer> _testcontainer(ArbeitenContainer::instance());

    //_df.insertarbeit("Sonstiges", "Dies ist ein Test", "Das Stichwort", "Erledigt", "SE", "Das sollte nur zum testen sein", _testlist, _testdozent.get());
    _df.updatearbeit(1, "Sonstiges", "Ein neuer Titel", "Ein neues Stichwort", "Erledigt", "SE", "Es hat sich was geändert", _testlist, _testdozent.get());
    return 0;
}
