#include "datenbankerstellung.h"

#include<QtSql>
#include<QList>
#include<QDebug>
#include<QFile>
#include<QTextStream>

//Wird ausnahmsweise verwendet, ist allgemein nicht empfohlen aufgrund von Namespace-Überlagerung!

DatenbankErstellung::DatenbankErstellung()
{
    //Erst die Datei laden
    QFile loadfile(_path + "/config.ini");
    if(loadpath() == "")
    {
        _path = QDir::currentPath();
        savepath();
        //Nochmal versuchen
        loadpath();
    }

    if(loadfile.open(QIODevice::ReadWrite) && loadfile.size() != 0)
    {
        connectdatabase();
    }
    else
    {
        createdatabase();
    }
}

DatenbankErstellung::~DatenbankErstellung()
{
}

bool DatenbankErstellung::createdatabase()
{
    QSqlDatabase mydb;
        mydb = QSqlDatabase::addDatabase("QSQLITE");
        mydb.setDatabaseName(_path + "/verwaltungsdb.sqlite");

        if (! mydb.open()) {
            return false;
        }

        QString q1 = "CREATE TABLE Admin ("
                     "Email_Admin varchar(50) primary key not null,"
                     "Kennwort varchar(50) not null);";

        QString q2 = "CREATE TABLE Dozent ("
                    "Email_Dozent varchar(50) primary key not null,"
                    "Vorname varchar(50) not null,"
                    "Nachname varchar(50) not null,"
                    "Kennwort varchar(50) not null);";

        QString q3 = "CREATE TABLE Bearbeiter ("
                     "Benutzer_id int primary key,"
                     "Name varchar(50));";

        QString q4 = "CREATE TABLE Arbeit ("
                                            "Arbeit_id int primary key not null,"
                                            "Email_Dozent varchar(50) references Dozent not null,"
                                            "Studiengang varchar(50) not null,"
                                            "Titel varchar(100) not null,"
                                            "Art varchar(20) not null,"
                                            "Stichwortliste varchar(150) not null,"
                                            "Status varchar(50) not null,"
                                            "Erlaeuterung varchar(100));";

        QString q5 = "CREATE TABLE Abschlussarbeit ("
                        "Arbeit_id int primary key references Arbeit not null,"
                        "Firma varchar(50),"
                        " bis date not null,"
                        " von date not null );";

        QString q6 = "CREATE TABLE Projektarbeit ("
                    " Arbeit_id int primary key references Arbeit not null,"
                    " Semester varchar(20) not null,"
                    " Schwerpunkt varchar(10) not null);";

        QString q7 = "CREATE TABLE bearbeitung("
                    "Benutzer_id int,"
                    "Arbeit_id int,"
                    "foreign key (Arbeit_id) references Arbeit,"
                    "foreign key (Benutzer_id) references Bearbeiter,"
                    "primary key(Benutzer_id,Arbeit_id));";

        QList<QString> list;
        list.append(q1);
        list.append(q2);
        list.append(q3);
        list.append(q4);
        list.append(q5);
        list.append(q6);
        list.append(q7);

        QString s;
        foreach (s , list) {
            QSqlQuery query;
            if (! query.exec(s)) {
                return false;
            }
        }
        savepath();
        return true;
}

bool DatenbankErstellung::connectdatabase()
{
    QSqlDatabase mydb;
        mydb = QSqlDatabase::addDatabase("QSQLITE");
        mydb.setDatabaseName(_path + "/verwaltungsdb.sqlite");

        if (! mydb.open()) {
            return false;
        }
        return true;
}

bool DatenbankErstellung::savepath()
{
    if(_path == "")
    {
        //Wenn der Pfad leer ist, setze auf Standardpfad
        _path = QDir::currentPath();
    }

    QFile _file(_path + "/config.ini");
    QTextStream _filestream(&_file);
    //Datei öffnen und leeren
    if(_file.open(QIODevice::WriteOnly))
    {
        _filestream << "Datenbankpfad:\n" << _path;
        //Wenn alles gepasst hat, passe den Pfad an und gib true zurück.
        _file.close();
        return true;
    }
    else
    {
        _file.close();
        return false;
    }
}

bool DatenbankErstellung::editpath(QString &_path)
{
    //Wenn der Pfad leer ist, macht das editieren keinen Sinn, deshalb false.
    if(_path == "")
    {
        return false;
    }
    else
    {
        QString _oldpath = this->_path;
        this->_path = _path;
        //Ansonsten, wenn er gespeichert werden kann, ändere den Pfad und gib true zurück.
        if(savepath())
        {
            return true;
        }
        else
        {
            this->_path = _oldpath;
            return false;
        }
    }
}

QString DatenbankErstellung::loadpath()
{
    QFile _file(_path + "/config.ini");
    //Nur zum lesen öffnen(QIODevice::Text ist default)
    if(_file.open(QIODevice::ReadOnly))
    {
        QTextStream _filestream(&_file);
        //Verwerfe die erste Zeile
        _path = _filestream.readLine();
        //Und lies die zweite ein
        _path = _filestream.readLine();
        //Schließe die File und gib den Pfad zurück
        _file.close();
        return _path;
    }
    else
    {
        //Ansonsten nehme den aktuellen Pfad
        _path = QDir::currentPath();
        _file.close();
        return _path;
    }
}
