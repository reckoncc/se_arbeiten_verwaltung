/*!
  @Author: Marina Mene Tedayem
  Die Klasse ArbeitenContainer wird in dieser .cpp Datei implementiert.
  Für die Datenbankanbindung wird das QtSql-Modul verwendet. Es bietet viele hilreiche
  Funktionen, die den Zugriff auf die Datenbank erleichtert.
  */
#include "ArbeitenContainer.h"
#include "Data/Administrator.h"
#include "./Data/Dozent.h"

#include <QtSql>

/*!
 * \brief ArbeitenContainer::instance
 * \return
 * Da der Konstruktor von der Klasse ArbeitenContainer privat ist, braucht man eine öffentliche
 * Methode, um auf die einzige Instanz dieser Klasse zugreifen zu können.
 * Wenn noch kein Objekt der Klasse ArbeitenContainer existiert, wird ein neues erzeugt.
 * Am Ende wird es zurückgegeben.
 */

ArbeitenContainer* ArbeitenContainer:: instance(){
    static ArbeitenContainer* _instance;
    if (_instance == 0){
        _instance = new ArbeitenContainer();
    }
    return _instance;
}

/*!
 * \brief ArbeitenContainer::insert
 * \param Arbeit &
 * \return int
 * Die Insert-Methode dient zum Eintragen von Arbeiten in der Datenbank.
 * Wenn eine neue Arbeit registriert werden soll, wird sie in der Tabelle Arbeit in der Datenbank
 * gespeichert.
 * Zudem wird dabei auch die Beziehung zwischen einer Arbeit und ihren Bearbeitern in der Datenbank
 * gespeichert. Dafür wird die IDs der eingetragenen Arbeit und des dazugehörigen Bearbeiters in der Tabelle
 * Bearbeitung gespeichert.
 * Da jede Arbeit von genau einem Dozenten betreuet wird, wird die Email des Betreuers gleich in der
 * Tabelle Arbeit gespeichert.
 * Am Ende wird die Id der gerade eingetragenen Arbeit zurückgegeben, wenn das Einfügen erfolgreich war.
 * Ansonsten wird -1 zurückgegeben.
 *
 */

int ArbeitenContainer:: insert(Arbeit *a){
    bool answer;
    int _id = a->arbeit_id();
    QString art = a->art();
    QString titel = a->titel();
    QString status=a->status();
    QString stg = a->studiengang();
    QString erl = a->erlaeuterung();
    QList<QString> bearbeiter;
    for(auto it : a->bearbeiter())
    {
        bearbeiter.append(it);
    }
    QString stch = a->stichwortliste();
    Dozent * betreuer = a->betreuer();
    QSqlQuery query1, query2;

    //insert values into Arbeit

    query1.prepare("INSERT INTO Arbeit (Arbeit_ID, Email_Dozent,Studiengang,Titel,Art,Status,Stichwortliste,Erlaeuterung)"
                  "VALUES(:id,:mail,:stg,:titel,:art,:stat,:stich,:erl)");
    query1.bindValue(":id", _id);
    query1.bindValue(":stg",stg);
    query1.bindValue(":titel",titel);
    query1.bindValue(":art",art);
    query1.bindValue(":stat",status);
    query1.bindValue(":stich", stch);
    query1.bindValue(":mail",betreuer->getmail());
    query1.bindValue(":erl",erl);

    int arbeit_nr ;
    if (query1.exec()){
        qDebug()<<"Insertion in Arbeit successfully";
        answer = true;
        arbeit_nr= query1.lastInsertId().toString().toInt();

        //Insert values into bearbeitung
        QString b;
        foreach (b, bearbeiter ){


             query2.prepare("INSERT INTO Bearbeitung(benutzer_id,arbeit_id) VALUES(:b,:a)");
             query2.bindValue(":b",b);
             query2.bindValue(":a",a->arbeit_id());
             if (query2.exec()){
                 qDebug()<<"cool";

             }
             else{
                 qDebug()<<"error by inserting values in bearbeitung";
                 return -1;
             }


        }

    }


    else {
        qDebug()<<"error by inserting values in Arbeit";
        qDebug() << query1.lastError();
        return -1;
    }


     return arbeit_nr;
}

/*!
 * \brief ArbeitenContainer::remove
 * \param int id
 * \return bool answer
 * Die Remove-Methode löscht von der Datenbank die Arbeit, mit der übergebenen Id.
 * Da die ID von jeder Arbeit eindeutig ist, benutzen wir die Spalte Arbeit_id der Datenbank,
 * um jede Arbeit zu identifizieren.
 * Zusätlich müssen noch alle Beziehungen mit dieser Arbeit in der Tabelle Bearbeitung gelöscht werden.
 * Dafür müssen alle Zeilen in der Tabelle Bearbeitung gelöscht werden, die die ID dieser Arbeit
 * enthalten.
 */
bool ArbeitenContainer:: remove(int id){

    QSqlQuery query1,query2;

    query1.prepare("Delete from Arbeit where arbeit_id = ?");
    query1.addBindValue(id);
    if (query1.exec()){
        qDebug()<< "Loeschen erfolgreich in Arbeit";
        query2.prepare("Delete from Bearbeitung where arbeit_id = ?");
        query2.addBindValue(id);
        if (query2.exec()){
            qDebug()<< "Loeschen erfolgreich in Bearbeitung";
            return true;
        }
    }
    else {
        qDebug()<< query2.lastError();
        return false;
    }
    return false;
}
/*!
 * \brief ArbeitenContainer::update
 * \param id
 * \param a
 * \return bool
 * Die Methode update im ArbeitenContainer dient zur Aktualisierung einer Arbeit, die schon in der
 * Datenbank eingetragen wurde.
 * Dazu wird den SQL-Befehl Update benutzt,um die Spalten der Tabelle Arbeit zu aktualisieren.
 */
bool ArbeitenContainer::update(int id,Arbeit *a){
    QSqlQuery query;
    query.prepare("UPDATE Arbeit SET Studiengang =:stg, Titel =:titel,Art =:art, Status =:stat, "
                  "Stichwortliste =:stwort, Email_dozent =:mail, erlaeuterung =:erl "
                  "WHERE arbeit_id =:id ");
    query.bindValue(":stg",a->studiengang());
    query.bindValue(":titel",a->titel());
    query.bindValue(":art",a->art());
    query.bindValue(":stat",a->status());
    query.bindValue(":stwort",a->stichwortliste());
    query.bindValue(":mail",a->betreuer()->getmail());
    query.bindValue(":erl",a->erlaeuterung());
    query.bindValue(":id",id);

    if (query.exec()){
        qDebug()<<"Cool by updating in Arbeit";
        return true;
    }

    else{
        qDebug()<< query.lastError();
        return false;
    }
}
/*!
 * \brief ArbeitenContainer::search
 * \param id
 * \return
 */
bool ArbeitenContainer :: search(int id){

    QSqlQuery query;
    query.prepare("Select * from Arbeit where arbeit_id = ?");
    query.addBindValue(id);
    query.exec();
    if (query.next()){
        qDebug()<<"Arbeit in der DB vorhanden!";
        return true;
    }
    else {
        qDebug()<<"Arbeit nicht in der DB.";
        return false;
    }
}

//Implementiert by Nils
QList<int> ArbeitenContainer::select(const QString & _type, const QString & _searched)
{
    QList<int> _gesuchtearbeiten{};
    QSqlQuery _query;
    //Nachdem alle IDs geholt wurden, vergleichen
    QString _newsearched = "%" + _searched + "%";
    _query.prepare("SELECT Arbeit_Id FROM Arbeit WHERE :type LIKE :searched");
    _query.bindValue(":type", _type);
    _query.bindValue(":searched", _newsearched);

    //Jetzt ausführen
    if(_query.exec())
    {
       for(auto i = 0; _query.next(); i++)
       {
           _gesuchtearbeiten.append(_query.value(i).toInt());
       }
       return _gesuchtearbeiten;
    }
    else
    {
        qDebug() << _query.lastError();
        return _gesuchtearbeiten;
    }

}

QList<Arbeit> select_by_id(QList<int> _ids)
{
    Arbeit _temparbeit = Arbeit();
    Administrator _admin = Administrator::get_admin();
    QSqlQuery _query;
    _query.prepare("SELECT * FROM Arbeit WHERE arbeit_id = :value");
    QList<Arbeit> _ergebnisse{};
    //Anm.: Da wir pro Durchlauf eine Arbeit erstellen, laufen wir jede query mehrmals durch.
    for(auto it : _ids)
    {
        _query.bindValue(":value", it);
        if(_query.exec() && _query.next())
        {
            _temparbeit.set_arbeit_id(_query.value(0).toInt());
            _temparbeit.set_betreuer(_admin.search_dozent(_query.value(1).toString()));
            _temparbeit.set_studiengang(_query.value(2).toString());
            _temparbeit.set_titel(_query.value(3).toString());
            _temparbeit.set_art(_query.value(4).toString());
            _temparbeit.set_stichwortliste(_query.value(5).toString());
            _temparbeit.set_status(_query.value(6).toString());
            _temparbeit.set_erlaeuterung(_query.value(7).toString());
            _ergebnisse.append(_temparbeit);
        }
        _query.clear();
        _query.prepare("SELECT * FROM Arbeit WHERE arbeit_id = :value");
    }
    return _ergebnisse;
}
