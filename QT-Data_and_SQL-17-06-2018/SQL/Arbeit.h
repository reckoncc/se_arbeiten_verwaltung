#ifndef ARBEIT
#define ARBEIT

#include <QCoreApplication>
#include <QString>
#include <QDate>

class Dozent;

class Arbeit {
public:
    Arbeit();

    Arbeit(QString art, QString titel, QString stwort, QString stat, QString stg, QString erl,
           QList<QString> bearbeiter,Dozent *d);

    // Getters
    QString art();
    QString titel();
    QString status();
    QString studiengang();
    QString erlaeuterung();
    int arbeit_id();
    QList<QString> bearbeiter();
    QString stichwortliste();
    Dozent * betreuer();


    //Setters
    void set_art(QString a);
    void set_titel(QString t);
    void set_status(QString s);
    void set_studiengang(QString stg);
    void set_erlaeuterung(QString erl);
    void set_stichwortliste(QString neu);
    void set_bearbeiter(QList<QString> neue);
    void set_betreuer(Dozent * d);
    void set_arbeit_id(int neu_id);

    //Destructor
    ~Arbeit(){ }


private:
    QString _art;
    QString _titel;
    QString _erlaeuterung;
    QString _stichwortliste;
    QString _studiengang;
    QString _status;
    int _arbeit_id;
    QList<QString> _bearbeiter;
    Dozent * _betreuer;
    int auto_id();

};




#endif // ARBEIT

