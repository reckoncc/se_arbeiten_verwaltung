#ifndef DATENBANKERSTELLUNG_H
#define DATENBANKERSTELLUNG_H

#include<memory>
#include<QString>
#include<QDir>

class DatenbankErstellung
{
private:
    //Variablen
    QString _path = QDir::currentPath();
    //Interne Funktionen
    QString  loadpath();
public:
    DatenbankErstellung();
    ~DatenbankErstellung();
    bool createdatabase();
    bool connectdatabase();
    bool savepath();
    bool editpath(QString & _path);
};

#endif // DATENBANKERSTELLUNG_H
