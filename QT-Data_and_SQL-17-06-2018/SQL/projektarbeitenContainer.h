#ifndef PROJEKTARBEITENCONTAINER_H
#define PROJEKTARBEITENCONTAINER_H

#include "Projektarbeit.h"
#include<QList>
#include <QString>


class ProjektarbeitenContainer {
public:
    static ProjektarbeitenContainer* instance_projektarbeiten();

    ProjektarbeitenContainer(ProjektarbeitenContainer const&)	= delete;
    void operator= (ProjektarbeitenContainer const&)	= delete;

    static QList<Projektarbeit *> arbeiten() {
        return _arbeiten;
    }

    int anzahl(){
        return _anzahl;
    }

     //Datenbank-Methoden
    bool insertProjektarbeit(Projektarbeit * a);
    bool updateProjektarbeit(int id,Projektarbeit * a);
    bool removeProjektarbeit(int id);
    bool searchProjektarbeit(int id) ;
    QList<Projektarbeit *> selectProjektarbeit(const QString s) const;


private:
    ProjektarbeitenContainer() {
        _anzahl = 0;
    }

    static QList<Projektarbeit *> _arbeiten;
    int _anzahl;
};
#endif // PROJEKTARBEITENCONTAINER_H

