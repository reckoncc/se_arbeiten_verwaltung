#include "Arbeit.h"
#include <QCoreApplication>
#include <QtSql>

Arbeit::Arbeit()
{
}

Arbeit::Arbeit(QString art, QString titel, QString stwort, QString stat, QString stg, QString erl,
               QList<QString> bearbeiter,Dozent* d){
    this->_art = art;
    this->_titel = titel;
    this->_stichwortliste = stwort;
    this->_status = stat;
    this->_studiengang = stg;
    this->_erlaeuterung = erl;
    for(auto it : bearbeiter)
    {
        this->_bearbeiter.append(it);
    }
    this->_betreuer = d;
    _arbeit_id = auto_id() + 1;
}

QString Arbeit::art(){ return _art; }
QString Arbeit::titel(){ return _titel; }
int Arbeit::arbeit_id(){ return _arbeit_id; }
QString Arbeit::status(){ return _status; }
QString Arbeit::studiengang(){ return _studiengang; }
QString Arbeit::erlaeuterung(){ return _erlaeuterung; }
QList<QString> Arbeit::bearbeiter(){ return _bearbeiter; }
QString Arbeit:: stichwortliste(){ return _stichwortliste; }
Dozent * Arbeit:: betreuer() { return _betreuer; }


void Arbeit::set_art(QString a){ _art = a; }
void Arbeit::set_titel(QString t){ _titel = t; }
void Arbeit::set_status(QString s){ _status = s; }
void Arbeit::set_studiengang(QString stg){ _studiengang = stg; }
void Arbeit::set_erlaeuterung(QString erl){ _erlaeuterung = erl; }
void Arbeit::set_stichwortliste(QString neu){ _stichwortliste = neu; }
void Arbeit::set_bearbeiter(QList<QString> neue){ _bearbeiter = neue; }
void Arbeit:: set_betreuer(Dozent * d){ _betreuer = d;}
void Arbeit::set_arbeit_id(int neu_id){ _arbeit_id = neu_id; }

//Interne ID-Funktion
int Arbeit::auto_id()
{
    QSqlQuery _query;
    int _lastvalue = 0;
    _query.prepare("SELECT max(Arbeit_Id) FROM Arbeit");
    if(_query.exec() && _query.first())
    {
        _lastvalue = _query.value(0).toInt();
        return _lastvalue;
    }
    else
    {
        return 0;
    }
}
