#include "abschlussarbeitenContainer.h"
#include "ArbeitenContainer.h"
#include "abschlussarbeit.h"
#include "Datum.h"

#include <QDebug>
#include <QtSql>

AbschlussarbeitenContainer* AbschlussarbeitenContainer::instance_abschlussarbeiten(){
    static AbschlussarbeitenContainer* _instance;
    if (_instance == 0){
        _instance = new AbschlussarbeitenContainer();
    }
    return _instance;
}

bool AbschlussarbeitenContainer::insertAbschlussarbeit(Abschlussarbeit *ab){
        Arbeit *arbeit = new Arbeit(ab->art(),ab->titel(),ab->stichwortliste(),ab->status(),ab->studiengang(),
                                   ab->erlaeuterung(),ab->bearbeiter(),ab->betreuer());

        int arbeit_id = ArbeitenContainer::instance()->insert(arbeit);
       if ( arbeit_id != -1){

           QSqlQuery query;
           QString bisstr, vonstr;
           Datum bisdat = Datum(ab->bis().jahr(), ab->bis().monat(), ab->bis().tag());
           Datum vondat = Datum(ab->von().jahr(), ab->von().monat(), ab->von().tag());
           bisdat = ab->bis();
           vondat = ab->von();
           bisstr = QString(QString::number(bisdat.jahr()) + "-" + QString::number(bisdat.monat()) + "-" + QString::number(bisdat.tag()));
           vonstr = QString(QString::number(vondat.jahr()) + "-" + QString::number(vondat.monat()) + "-" + QString::number(vondat.tag()));

           query.prepare("INSERT INTO Abschlussarbeit (arbeit_id,firma,bis,von)"
                         "VALUES (:id,:f,:b,:v)");
           query.bindValue(":id",arbeit_id);
           query.bindValue(":f",ab->firma());
           query.bindValue(":b",bisstr);
           query.bindValue(":v",vonstr);

           if (query.exec()){
               qDebug()<<"cool by inserting in Abschlussarbeit!";
               return true;
           }
       }
       else{
           qDebug()<<"Uncool while inserting in Abschlussarbeit!";
           return false;
       }
    return false;
}

bool AbschlussarbeitenContainer:: updateAbschlussarbeit(int id, Abschlussarbeit * ab) {

    Arbeit arbeit = *new Arbeit(ab->art(),ab->titel(),ab->stichwortliste(),ab->status(),ab->studiengang(),
                               ab->erlaeuterung(),ab->bearbeiter(),ab->betreuer());
    bool answer = ArbeitenContainer::instance()->update(id,&arbeit);
    if (answer){
        QSqlQuery query;
        QString bisstr, vonstr;
        Datum bisdat = Datum(ab->bis().jahr(), ab->bis().monat(), ab->bis().tag());
        Datum vondat = Datum(ab->von().jahr(), ab->von().monat(), ab->von().tag());
        bisdat = ab->bis();
        vondat = ab->von();
        bisstr = QString(QString::number(bisdat.jahr()) + "-" + QString::number(bisdat.monat()) + "-" + QString::number(bisdat.tag()));
        vonstr = QString(QString::number(vondat.jahr()) + "-" + QString::number(vondat.monat()) + "-" + QString::number(vondat.tag()));
        query.prepare("UPDATE Abschlussarbeit Set firma=:f, bis=:b, von=:v "
                      "WHERE arbeit_id =:id");
        query.bindValue(":f",ab->firma());
        query.bindValue(":b",bisstr);
        query.bindValue(":v",vonstr);
        query.bindValue(":id",id);

        if (query.exec()){
            qDebug()<<"Cool update in Abschlussarbeit";
            return true;
        }
        else {
            qDebug()<<"Uncool update in Abschlussarbeit";
            return false;
        }
    }
    else{

        qDebug()<<"Uncool update in Abschlussarbeit";
        return false;
    }


}

bool AbschlussarbeitenContainer:: removeAbschlussarbeit(int id){
    QSqlQuery query;
    query.prepare("Delete From Abschlussarbeit where arbeit_id = ?");
    query.addBindValue(id);
    if (query.exec()){
        qDebug()<<"Cool delete in Abschlussarbeit";
        return ArbeitenContainer::instance()->remove(id);

    }
    else{
        return false;
    }
}
bool AbschlussarbeitenContainer::searchAbschlussarbeit(int id){
    QSqlQuery query;
    query.prepare("Select * from Abschlussarbeit where arbeit_id = ?");
    query.addBindValue(id);
    query.exec();
    if (query.next()){
        qDebug()<<"Abschlussarbeit in der DB vorhanden!";
        return true;
    }
    else {
        qDebug()<<"Abschlussarbeit nicht in der DB.";
        return false;
    }
}

QList<Abschlussarbeit *>selectAbschlussarbeit(const QString & s)
{
    QList<Abschlussarbeit*>test;
    return test;
}
