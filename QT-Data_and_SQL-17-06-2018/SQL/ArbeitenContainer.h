/*!
  @Author: Marina Mene Tedayem
  Die Klasse ArbeitenContainer speichert alle Arbeiten, die in der Datenbank gespeichert werden,
  seien es Projektarbeiten, Abschlussarbeiten oder sonstige Arbeiten.
  Sie stellt Funktionen für die Verwaltung von Arbeiten zur Verfügung, sowie das Einfügen,
  das Aktualisieren und das Löschen von sämtlichen Arbeiten und auch das Suchen nach bestimmten
  Kriterien in der Datenbank.
  Für die Speicherung von Arbeiten wird ein Vektor benutzt.
  Da wir nur einen einzigen Container für die Objekten brauchen, benutzen wir das Singleton Muster.
  Konkret heißt es, dass nur eine einzige Instanz von diesem Container erzeugt werden kann.
  Dies ist möglich, wenn der Konstruktor dieser Klasse privat ist. Zusätlich brauchen wir auch eine Variable
  _anzahl, um die Anzahl der eingetragen Arbeiten zu speichern.
  */
#ifndef ARBEITENCONTAINER_H_
#define ARBEITENCONTAINER_H_

#include "Arbeit.h"

class ArbeitenContainer {
public:
    static ArbeitenContainer* instance();

    ArbeitenContainer(ArbeitenContainer const&)	= delete;
    void operator= (ArbeitenContainer const&)	= delete;

    static QList<Arbeit *> arbeiten() {
        return _arbeiten;
    }

    int anzahl(){
        return _anzahl;
    }

     //Datenbank-Methoden
    int insert(Arbeit * a);
    bool update(int id,Arbeit * a);
    bool remove(int id);
    bool search(int id) ;

    QList<int> select(const QString & _type, const QString & _searched);
    QList<Arbeit> select_by_id(QList<int> _ids);


private:
    ArbeitenContainer() {
        _anzahl;
    }

    static QList<Arbeit *> _arbeiten;
    int _anzahl;
};

#endif /* ARBEITENCONTAINER_H_ */
