#ifndef DOZENTENCONTAINER_H
#define DOZENTENCONTAINER_H

#include<QString>
#include <QList>
/*!
 * \brief The DozentenContainer class
 * \author Marina MENE TEDAYEM
 * \details Diese Klasse dient als Container, um Dozenten zu speichern und verwalten.
 * Sie benutzt das Singleton Pattern, da wir einen einzigen Container brauchen.
 * Sie stellt sämtliche Methoden und Attributen zur Verfügung, damit die Dozenten
 * gemütlich in der Datenbank eingefügt, aktualisiert und gelöscht werden können.
 */
class Dozent;

class DozentenContainer
{
public:
    static DozentenContainer * dozentenContainer();

    DozentenContainer(DozentenContainer const&)	= delete;
    void operator= (DozentenContainer const&)	= delete;

    /*!
     *Die Klasse besitzt auch einen leeren Destruktor, der aufgerufen wird, wenn der Container
     * gelöscht werden sollte.
     */
    ~DozentenContainer(){}

    QList<Dozent *> get_list_dozenten();

    bool insert(QString vorname, QString nachname, QString mail, QString hashedPsw);
    bool remove (QString mail);
    bool update(QString mail,QString neu_mail,QString neu_nachname,QString neu_vorname,QString neu_kennwort);
    Dozent * getDozent(QString mail);

private:
    /*!
     * \brief DozentenContainer
     * \details Die Klasse DozentenContainer besitzt einen privaten leeren Container, damit weitere Objekte
     * nicht erzeugt werden können.
     */
    DozentenContainer() {

    }
    /*!
     * \brief list_dozenten
     * \details Diese Liste enthält Zieger auf alle Objekte der Klasse Dozent.
     */
    QList<Dozent *> list_dozenten;

};

#endif // DOZENTENCONTAINER_H

