#ifndef ABSCHLUSSARBEIT_H
#define ABSCHLUSSARBEIT_H

#include "Arbeit.h"
#include "Datum.h"

#include <QString>
#include <QList>

#include <memory>

class Dozent;

class Abschlussarbeit : public Arbeit
{
public:
    Abschlussarbeit(QString art, QString titel, QString stwort, QString stat, QString stg, QString erl, QList<QString> bearbeiter, Dozent * doz,  QString f, Datum * v, Datum * b);

    ~Abschlussarbeit();

    QString& firma(){return _firma;}
    void set_firma(QString f){this->_firma = f;}

    Datum& von(){return *_von;}
    void set_von(Datum * v){this->_von.reset(v);}

    Datum& bis(){return *_bis;}
    void set_bis(Datum * b){this->_bis.reset(b);}

private:
    QString _firma;
    std::unique_ptr<Datum>_von;
    std::unique_ptr<Datum> _bis;
};

#endif // ABSCHLUSSARBEIT_H
