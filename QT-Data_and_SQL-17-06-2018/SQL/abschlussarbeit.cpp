#include "abschlussarbeit.h"

Abschlussarbeit::Abschlussarbeit(QString art, QString titel, QString stwort, QString stat, QString stg, QString erl,
                                                  QList<QString> bearbeiter, Dozent * doz,  QString f, Datum * v, Datum * b)
                                                  : Arbeit(art, titel, stwort, stat, stg, erl, bearbeiter, doz)
{
      this->_firma = f;
      this->_von.reset(v);
      this->_bis.reset(b);
}

Abschlussarbeit::~Abschlussarbeit()
{

}
