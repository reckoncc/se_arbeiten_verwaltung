#ifndef ABSCHLUSSARBEITENCONTAINER_H
#define ABSCHLUSSARBEITENCONTAINER_H

#include <QList>
#include <QString>

class Datum;
class Abschlussarbeit;

class AbschlussarbeitenContainer {
public:
    static AbschlussarbeitenContainer* instance_abschlussarbeiten();

    AbschlussarbeitenContainer(AbschlussarbeitenContainer const&)	= delete;
    void operator= (AbschlussarbeitenContainer const&)	= delete;

    static QList<Abschlussarbeit *> arbeiten() {
        return _arbeiten;
    }

    int anzahl(){
        return _anzahl;
    }

     //Datenbank-Methoden
    bool insertAbschlussarbeit(Abschlussarbeit * a);
    bool updateAbschlussarbeit(int id,Abschlussarbeit * a);
    bool removeAbschlussarbeit(int id);
    bool searchAbschlussarbeit(int id) ;
    QList<Abschlussarbeit *> selectAbschlussarbeit(const QString &s);


private:
    AbschlussarbeitenContainer() {
        _anzahl = 0;
    }

    static QList<Abschlussarbeit *> _arbeiten;
    int _anzahl;
};
#endif // ABSCHLUSSARBEITENCONTAINER_H

