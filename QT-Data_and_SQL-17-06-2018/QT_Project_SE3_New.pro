#-------------------------------------------------
#
# Project created by QtCreator 2018-06-12T13:25:46
#
#-------------------------------------------------

QT       += core gui widgets sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QT_Project_SE3_New
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
    GUI/loginwindow.cpp \
    GUI/loginwindow.cpp \
    Data/Administrator.cpp \
    SQL/abschlussarbeit.cpp \
    SQL/abschlussarbeitenContainer.cpp \
    SQL/Arbeit.cpp \
    SQL/ArbeitenContainer.cpp \
    SQL/Datum.cpp \
    SQL/dozentenContainer.cpp \
    SQL/projektarbeit.cpp \
    SQL/projektarbeitenContainer.cpp \
    Data/Dozent.cpp \
    Data/Student.cpp \
    SQL/datenbankerstellung.cpp \
    Data/datenfacade.cpp

HEADERS += \
    GUI/loginwindow.h \
    Data/Administrator.h \
    SQL/abschlussarbeit.h \
    SQL/abschlussarbeitenContainer.h \
    SQL/Arbeit.h \
    SQL/ArbeitenContainer.h \
    SQL/Datum.h \
    SQL/dozentenContainer.h \
    SQL/projektarbeit.h \
    SQL/projektarbeitenContainer.h \
    Data/Dozent.h \
    Data/Student.h \
    SQL/datenbankerstellung.h \
    Data/datenfacade.h
