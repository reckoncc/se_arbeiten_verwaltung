#ifndef LOGINWINDOW_H
#define LOGINWINDOW_H

#include <QMainWindow>

#include <memory>

//Alle Klassen vorweg definiert für den Compiler
class AdminLogin;
class DozentLogin;
class StudentenLogin;

class LoginWindow : public QMainWindow
{
    Q_OBJECT

public:
    LoginWindow(QWidget *parent = 0);
    ~LoginWindow();

private:
    std::shared_ptr<AdminLogin> _adminlogin;
    std::shared_ptr<DozentLogin> _dozentlogin;
    std::shared_ptr<StudentenLogin> _studentenlogin;
};

#endif // LOGINWINDOW_H
