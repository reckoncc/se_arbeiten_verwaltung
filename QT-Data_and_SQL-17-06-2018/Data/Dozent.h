#ifndef DOZENT_H
#define DOZENT_H

#include "Student.h"

#include<QString>
#include<QList>
#include<QDate>

#include<memory>

//Vorabdefinition der Container Klassen
class ArbeitenContainer;
class AbschlussarbeitenContainer;
class ProjektarbeitenContainer;

class Dozent:public Student
{
    public:
        //Wird aufgrund spezieller instanzierung benötigt
        Dozent();
        //Anmerkung: Das Passwort muss beim erstellen bereits gehashed sein!
        Dozent(const QString & _vorname, const QString & _nachname, const QString & _mail, const QString & _password);
        //Dozent-Konstruktor-Forwarding
        Dozent(QString && _vorname, QString && _nachname, QString && _mail, QString && _password);
        //Destruktor
        ~Dozent();
        //DB Funktionen
        bool insert_arbeit(const char & _type, const QString & _art, const QString & _titel, const QString  & _stichwort, const QString & _status, const QString & _studiengang,
                                const QString & _beschreibung, QList<QString> & _bearbeiter, Dozent * _dozent, const QString & _firma = "", const QDate & _von = QDate::currentDate(),
                                const QDate & _bis = QDate::currentDate(), const QString & _schwerpunkt = "", const QString & _semester = "");
        //Funktion wird überladen, falls das Passwort nicht geändert wird
        bool update_arbeit(const char & _type, const int & _id, const QString & _art, const QString & _titel, const QString  & _stichwort, const QString & _status, const QString & _studiengang,
                           const QString & _beschreibung, QList<QString> & _bearbeiter, Dozent * _dozent, const QString & _firma = "", const QDate & _von = QDate::currentDate(),
                           const QDate & _bis = QDate::currentDate(), const QString & _schwerpunkt = "", const QString & _semester = "");
        
        bool delete_arbeit(const char & _type, const int & _id);
        QString& getmail(){return this->_mail;}
    private:
    //Die Eigenschaften des Dozenten
    QString _vorname;
    QString _nachname;
    QString _mail;
    QString _password;

};


#endif
