#include "Administrator.h"
#include "SQL/dozentenContainer.h"
#include "Dozent.h"

#include <utility>

Administrator::Administrator()
{
}

Administrator::~Administrator(){}

Administrator & Administrator::get_admin()
{
    static Administrator * _admin;
    if(_admin == nullptr)
    {
        _admin = new Administrator();
    }
    return *_admin;
}

bool Administrator::set_admin(const QString & _mail, const QString & _password)
{
    this->_mail = _mail;
    this->_password = _password;
    return true;
}

void Administrator::container_initialisieren()
{
    if(!(_dozentencontainer.isNull()))
    {
        _dozentencontainer.reset(DozentenContainer::dozentenContainer());
    }
}

bool Administrator::insert_dozent(const QString & _vorname, const QString & _nachname, const QString & _mail, const QString & _password)
{
    //Dozentencontainer initialisieren
    void container_initialisieren();
    //Jetzt wird das Passwort gehashed
    auto _hashedpassword = passwort_hashen(_password);
    //Jetzt in SQL einfügen
    if(_dozentencontainer->insert(_vorname, _nachname, _mail, _hashedpassword))
    {
        return true;
    }
    else
    {
        return false;
    }
}

//Funktion mit Passwordwechsel
bool Administrator::update_dozent(const QString & _oldmail, const QString & _vorname, const QString & _nachname, const QString & _mail, const QString & _password = "")
{
    //Dozentencontainer initialisieren
    void container_initialisieren();
    //Wenn ein neues Passwort übergeben wird, wird es gehashed
    auto _hashedpassword = passwort_hashen(_password);
    //Anm.: Wieso ist bei dieser Funktion die Reihenfolge anders? -> TODO: Marina fragen.
    if(!(_password == "") && _dozentencontainer->update(_oldmail, _mail, _nachname, _vorname, _hashedpassword))
    {
        return true;
    }
    else
    {
        _dozentencontainer->update(_oldmail, _mail, _nachname, _vorname, _password);
        return true;
    }
    return false;
}

//Funktion zum löschen
bool Administrator::delete_dozent(const QString & _mail)
{
    //Dozentencontainer initialisieren
    void container_initialisieren();
    if(_dozentencontainer->remove(_mail))
    {
        return true;
    }
    else
    {
        return false;
    }
}

Dozent * Administrator::search_dozent(const QString &_mail)
{
    container_initialisieren();
    return _dozentencontainer->getDozent(_mail);
}

//Interne Funktionen
QString Administrator::passwort_hashen(const QString & _password)
{
    if(!(_password == ""))
    {
        std::hash<std::string>hs;
        std::basic_string<char>bs = _password.toStdString();
        auto _hashtempsave = QString::number(hs(bs));
        return _hashtempsave;
    }
    else
    {
        return _password;
    }
}
