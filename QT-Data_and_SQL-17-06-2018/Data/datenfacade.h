#ifndef DATENFACADE_H
#define DATENFACADE_H

#include "Administrator.h"
#include "Dozent.h"
#include "Student.h"

#include <QSharedPointer>

class Arbeit;

class Datenfacade
{
private:
    //Da die Datenfacade nur einmal existieren darf, wird diese mittels Funktion erstellt und eine Referenz auf diese zurückgegeben
    Datenfacade();
public:
    //Offener Konstruktor
    ~Datenfacade();
    static Datenfacade & getfacade();
    //Sämtliche Funktionen, die in der Facade benötigt werden.

    /***************************************************
     *                           Administrator                                    *
     ***************************************************/
    //Funktionalitäten
    Administrator & getadmin();
    bool setadmin(const QString & _mail,const QString & _password);
    //Verwaltungsfunktionen
    bool insertdozent(const QString & _vorname, const QString & _nachname, const QString & _mail, const QString & _password);
    bool updatedozent(const QString & _oldmail, const QString & _vorname, const QString & _nachname, const QString & _mail, const QString & _password);
    bool deletedozent(const QString & _mail);

    /***************************************************
     *                                    Dozent                                    *
     ***************************************************/

    //Funktionalitäten
    Dozent createdozent(const QString & _vorname, const QString & _nachname, const QString & _email, const QString & _passwort);

    //Verwaltungsfunktionen
    //Einfügeoperationen
    //Einfügen von Sonstigen Arbeiten
    bool insertarbeit(const QString & _art, const QString & _titel, const QString  & _stichwort, const QString & _status, const QString & _studiengang,
                              const QString & _beschreibung, QList<QString> & _bearbeiter, Dozent * _dozent);

    //Einfügen von Projektarbeiten
    bool insertprojekt(const QString & _art, const QString & _titel, const QString  & _stichwort, const QString & _status, const QString & _studiengang,
                                const QString & _beschreibung, QList<QString> & _bearbeiter, Dozent * _dozent, const QString & _schwerpunkt, const QString & _semester);

    //Einfügen von Abschlussarbeiten
    bool insertabschluss(const QString & _art, const QString & _titel, const QString  & _stichwort, const QString & _status, const QString & _studiengang,
                                    const QString & _beschreibung, QList<QString> & _bearbeiter, Dozent * _dozent, const QString & _firma = "", const QDate & _von = QDate::currentDate(),
                                    const QDate & _bis = QDate::currentDate());

    //Update von Sonstigen Arbeiten
    bool updatearbeit(const int & _id, const QString & _art, const QString & _titel, const QString  & _stichwort, const QString & _status, const QString & _studiengang,
                      const QString & _beschreibung, QList<QString> & _bearbeiter, Dozent * _dozent);
    //Update von Projektarbeiten
    bool updateprojekt(const int & _id, const QString & _art, const QString & _titel, const QString  & _stichwort, const QString & _status, const QString & _studiengang,
                       const QString & _beschreibung, QList<QString> & _bearbeiter, Dozent * _dozent, const QString & _schwerpunkt = "", const QString & _semester = "");

    //Update von Abschlussarbeiten
    bool updateabschluss(const int & _id, const QString & _art, const QString & _titel, const QString  & _stichwort, const QString & _status, const QString & _studiengang,
                         const QString & _beschreibung, QList<QString> & _bearbeiter, Dozent * _dozent, const QString & _firma = "", const QDate & _von = QDate::currentDate(),
                         const QDate & _bis = QDate::currentDate());

    //Löschfunktion für alle Arbeiten
    bool deletearbeit(const int & _id);
    bool deleteprojekt(const int & _id);
    bool deleteabschluss(const int & _id);
    /***************************************************
     *                                   Student                                    *
     ***************************************************/
    QList<Arbeit> search_art(const QString & _gesucht);
    QList<Arbeit>  search_titel(const QString & _gesucht);
    QList<Arbeit>  search_schwerpunkt(const QString & _gesucht);
    QList<Arbeit>  search_bearbeiter(const QString & _gesucht);
    QList<Arbeit>  search_betreuer(const QString & _gesucht);
    QList<Arbeit>  search_stichworte(const QList<QString> & _gesucht);
    QList<Arbeit>  search_datum(const QDate & _von, const QDate & _bis);
};

#endif // DATENFACADE_H
