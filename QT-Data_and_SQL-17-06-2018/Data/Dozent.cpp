#include "Dozent.h"
#include "SQL/Arbeit.h"
#include "SQL/abschlussarbeit.h"
#include "SQL/projektarbeit.h"
#include "SQL/ArbeitenContainer.h"
#include "SQL/ProjektarbeitenContainer.h"
#include "SQL/AbschlussarbeitenContainer.h"
#include "SQL/Datum.h"

//Konstruktoren
Dozent::Dozent()
{
}

Dozent::Dozent(const QString & _vorname, const QString & _nachname, const QString & _mail, const QString & _password)
    : _vorname(_vorname), _nachname(_nachname), _mail(_mail), _password(_password)
{
}

Dozent::Dozent(QString && _vorname, QString && _nachname, QString && _mail, QString && _password)
    :_vorname(_vorname), _nachname(_nachname), _mail(_mail), _password(_password)
{
}

Dozent::~Dozent()
{
}

bool Dozent::insert_arbeit(const char & _type, const QString & _art, const QString & _titel, const QString  &_stichwort, const QString & _status, const QString & _studiengang, const QString & _beschreibung,
                   QList<QString> & _bearbeiter, Dozent * _dozent, const QString & _firma, const QDate & _von, const QDate & _bis, const QString & _schwerpunkt, const QString & _semester)
{

    //Wenn wir ein Projekt initialisieren
    //Anm: Da wir in den Case-Zweigen vereinzelte Variablen deklarieren, jedoch nicht immer initialisieren, sie trotzdem alle im selben Scope sind, müssen wir ihnen einen eigenen, lokalen Scope geben
    switch(_type)
    {
        case 'p':
        {

            if(!(_projektcontainer.get()))
            {
                //ProjektContainer initialisieren
                projekt_initialisieren();
            }
            std::unique_ptr<Projektarbeit> _projektarbeit(new Projektarbeit(_art, _titel, _stichwort, _status, _studiengang, _beschreibung, _bearbeiter, _dozent, _schwerpunkt, _semester));
            if(_projektcontainer->insertProjektarbeit(_projektarbeit.get()))
            {
                _projektarbeit.reset();
                return true;
            }
            else
            {
                _projektarbeit.reset();
                return false;
            }
        }
        case 'a':
        {
            if(!(_abschlusscontainer.get()))
            {
                //AbschlussContainer initialisieren
                abschluss_initialisieren();
            }
            //Notwendig, um von QDate auf den eigenen Typen Datum zu konvertieren.
            Datum * _convertedvon = new Datum(_von.year(), _von.month(), _von.day());
            Datum * _convertedbis = new Datum(_bis.year(), _bis.month(), _bis.day());
            std::unique_ptr<Abschlussarbeit> _abschlussarbeit(new Abschlussarbeit(_art, _titel, _stichwort, _status, _studiengang, _beschreibung, _bearbeiter, _dozent, _firma, _convertedvon, _convertedbis));
            if(_abschlusscontainer->insertAbschlussarbeit(_abschlussarbeit.get()))
            {
                _abschlussarbeit.reset();
                delete _convertedbis, _convertedvon;
                return true;
            }
            else
            {
                _abschlussarbeit.reset();
                delete _convertedbis, _convertedvon;
                return false;
            }
        }
        case 's':
        {
            if(!(_arbeitscontainer.get()))
            {
                //Arbeitscontainer initialisieren
                arbeiten_initialisieren();
            }
            std::unique_ptr<Arbeit> _sonstigearbeit(new Arbeit(_art, _titel, _stichwort, _status, _studiengang, _beschreibung, _bearbeiter, _dozent));
            if(_arbeitscontainer->insert(_sonstigearbeit.get()) == -1)
            //if(_arbeitscontainer->insert(_art, _titel, _stichwort, _status, _studiengang, _beschreibung, _bearbeiter, _dozent))
            {
                //_sonstigearbeit.reset();
                return true;
            }
            else
            {
                //_sonstigearbeit.reset();
                return false;
            }
         }
        default:
            //Sollte nie erreicht werden!!!
            return false;
    }
}

//Arbeit Updaten
bool Dozent::update_arbeit(const char & _type, const int & _id, const QString & _art, const QString & _titel, const QString  & _stichwort, const QString & _status, const QString & _studiengang,
                            const QString & _beschreibung, QList<QString> & _bearbeiter, Dozent * _dozent, const QString & _firma, const QDate & _von,
                            const QDate & _bis, const QString & _schwerpunkt, const QString & _semester)
{

    switch(_type)
    {
        case 'p':
        {
            if(!(_projektcontainer.get()))
            {
                //ProjektContainer initialisieren
                projekt_initialisieren();
            }
            std::unique_ptr<Projektarbeit> _projektarbeit(new Projektarbeit(_art, _titel, _stichwort, _status, _studiengang,
                                                                               _beschreibung, _bearbeiter, _dozent, _schwerpunkt, _semester));
            if(_projektcontainer->updateProjektarbeit(_id, _projektarbeit.get()))
            {
                _projektarbeit.reset();
                return true;
            }
            else
            {
                _projektarbeit.reset();
                return false;
            }
        }
        case 'a':
        {
            if(!(_abschlusscontainer.get()))
            {
                //AbschlussContainer initialisieren
                abschluss_initialisieren();
            }
            //Notwendig, um von QDate auf den eigenen Typen Datum zu konvertieren.
            Datum * _convertedvon = new Datum(_von.year(), _von.month(), _von.day());
            Datum * _convertedbis = new Datum(_bis.year(), _bis.month(), _bis.day());
            std::unique_ptr<Abschlussarbeit> _abschlussarbeit(new Abschlussarbeit(_art,_titel,_stichwort,_status,_studiengang,_beschreibung,
                                                                                                                  _bearbeiter,_dozent,_firma,_convertedvon,_convertedbis));
            if(_abschlusscontainer->updateAbschlussarbeit(_id, _abschlussarbeit.get()))
            {
                _abschlussarbeit.reset();
                return true;
            }
            else
            {
                return false;
            }
        }
        case 's':
        {
            if(!(_arbeitscontainer.get()))
            {
                //Arbeitscontainer initialisieren
                arbeiten_initialisieren();
            }
            std::unique_ptr<Arbeit> _sonstigearbeit(new Arbeit(_art,_titel,_stichwort,_status,_studiengang,_beschreibung,_bearbeiter,_dozent));
            if(_arbeitscontainer->update(_id, _sonstigearbeit.get()))
            {
                _sonstigearbeit.reset();
                return true;
            }
            else
            {
                return false;
            }
        }
    default:
        //Sollte nie erreicht werden
        return false;
    }
}

bool Dozent::delete_arbeit(const char & _type, const int &_id)
{
    switch(_type)
    {
        case 'p':
        {
            if(!(_projektcontainer.get()))
            {
                //ProjektContainer initialisieren
                projekt_initialisieren();
            }
            if(_projektcontainer->removeProjektarbeit(_id))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        case 'a':
        {
            if(!(_abschlusscontainer.get()))
            {
                //AbschlussContainer initialisieren
                abschluss_initialisieren();
            }
            if(_abschlusscontainer->removeAbschlussarbeit(_id))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        case 's':
        {
            if(!(_arbeitscontainer.get()))
            {
                //Arbeitscontainer initialisieren
                arbeiten_initialisieren();
            }
            if(_arbeitscontainer->remove(_id))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        default:
        //Darf nie erreicht werden!
        return false;
    }
}
