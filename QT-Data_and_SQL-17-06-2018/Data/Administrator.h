#ifndef ADMINISTRATOR_H
#define ADMINISTRATOR_H

#include <QString>
#include <QSharedPointer>

class DozentenContainer;
class Dozent;

class Administrator
{
    public:
        //Desktruktor
        ~Administrator();
        static Administrator & get_admin();
        bool set_admin(const QString & _mail, const QString & _password);
        //DB Funktionen
        bool insert_dozent(const QString & _vorname, const QString & _nachname, const QString & _mail, const QString & _password);
        //Funktion zum updaten
        bool update_dozent(const QString & _oldmail, const QString & _mail, const QString & _nachname, const QString & _vorname, const QString & _password);
        // Lösch-Funktion
        bool delete_dozent(const QString & _mail);
        //Suchfunktion
        Dozent * search_dozent(const QString & _mail);
        
    private:
        //Konstruktoren
        Administrator();
        //Variablen
        QString _mail;
        QString _password;
        //SharedPointer auf mehrfachverwendete Objekte
        QSharedPointer<DozentenContainer> _dozentencontainer;
        //Funktionen
        void container_initialisieren();
        QString passwort_hashen(const QString & _password);
};
#endif
