#include "Student.h"
#include "SQL/Arbeit.h"
#include "SQL/ArbeitenContainer.h"
#include "SQL/abschlussarbeitenContainer.h"
#include "SQL/projektarbeitenContainer.h"


Student::Student(){}

Student::~Student(){}

QList<Arbeit> Student::search_art(const QString &_searched)
{
    //Container initialisieren
    arbeiten_initialisieren();
    abschluss_initialisieren();
    projekt_initialisieren();

    QList<Arbeit> _ergebnisse;
    QList<int> _zwischenergebnis{};
    //Mit einer hergestellten Verbindung jetzt suchen und zurückbekommen
    _zwischenergebnis = _arbeitscontainer->select("Art",_searched);
    //_ergebnisse = _arbeitscontainer->select_by_id(_zwischenergebnis);
    return _ergebnisse;
}

QList<Arbeit> Student::search_bearbeiter(const QString &_searched)
{
    //Container initialisieren
    arbeiten_initialisieren();
    abschluss_initialisieren();
    projekt_initialisieren();

    QList<Arbeit> _ergebnisse;
    QList<int> _zwischenergebnis{};
    //Mit einer hergestellten Verbindung jetzt suchen und zurückbekommen
    _zwischenergebnis = _arbeitscontainer->select("Bearbeiter",_searched);
    //_ergebnisse = _arbeitscontainer->select_by_id(_zwischenergebnis);
    return _ergebnisse;
}

QList<Arbeit> Student::search_betreuer(const QString &_searched)
{
    //Container initialisieren
    arbeiten_initialisieren();
    abschluss_initialisieren();
    projekt_initialisieren();

    QList<Arbeit> _ergebnisse;
    QList<int> _zwischenergebnis{};
    //Mit einer hergestellten Verbindung jetzt suchen und zurückbekommen
    _zwischenergebnis = _arbeitscontainer->select("Betreuer",_searched);
    //_ergebnisse = _arbeitscontainer->select_by_id(_zwischenergebnis);
    return _ergebnisse;
}

/*QList<Arbeit> Student::search_datum(const QDate &_von, const QDate &_bis)
{
    //Container initialisieren
    arbeiten_initialisieren();
    abschluss_initialisieren();
    projekt_initialisieren();

    QList<Arbeit> _ergebnisse;
    QList<Arbeit> _endergebnis;
    QList<int> _zwischenergebnis{};
    //Mit einer hergestellten Verbindung jetzt suchen und zurückbekommen
    _zwischenergebnis = _arbeitscontainer->select("Datum",_von);
    _ergebnisse = _arbeitscontainer->select_by_id(_zwischenergebnis);
    _zwischenergebnis = _arbeitscontainer->select("Datum", _bis);
    _endergebnis = _arbeitscontainer->select_by_id(_zwischenergebnis);
    for(auto it : _ergebnisse)
    {
        _endergebnis.append(it);
    }
    return _ergebnisse;
} */

QList<Arbeit> Student::search_schwerpunkt(const QString &_searched)
{
    //Container initialisieren
    arbeiten_initialisieren();
    abschluss_initialisieren();
    projekt_initialisieren();

    QList<Arbeit> _ergebnisse;
    QList<int> _zwischenergebnis{};
    //Mit einer hergestellten Verbindung jetzt suchen und zurückbekommen
    _zwischenergebnis = _arbeitscontainer->select("Schwerpunkt",_searched);
    //_ergebnisse = _arbeitscontainer->select_by_id(_zwischenergebnis);
    return _ergebnisse;
}

QList<Arbeit> Student::search_stichworte(const QList<QString> &_searched)
{
    //Container initialisieren
    arbeiten_initialisieren();
    abschluss_initialisieren();
    projekt_initialisieren();

    QList<Arbeit> _ergebnisse;
    QList<Arbeit> _endergebnis;
    QList<int> _zwischenergebnis{};
    for(auto it : _searched)
    {
        //Mit einer hergestellten Verbindung jetzt suchen und zurückbekommen
        _zwischenergebnis = _arbeitscontainer->select("Stichworte",it);
        //_ergebnisse = _arbeitscontainer->select_by_id(_zwischenergebnis);
        for(auto _it : _ergebnisse)
        {
            _endergebnis.append(_it);
        }
    }
    return _ergebnisse;
}

QList<Arbeit> Student::search_titel(const QString &_searched)
{
    //Container initialisieren
    arbeiten_initialisieren();
    abschluss_initialisieren();
    projekt_initialisieren();

    QList<Arbeit> _ergebnisse;
    QList<int> _zwischenergebnis{};
    //Mit einer hergestellten Verbindung jetzt suchen und zurückbekommen
    _zwischenergebnis = _arbeitscontainer->select("Titel",_searched);
    //_ergebnisse = _arbeitscontainer->select_by_id(_zwischenergebnis);
    return _ergebnisse;
}

//Interne Funktionen
void Student::arbeiten_initialisieren()
{
    if(!(_arbeitscontainer.get()))
    {
        _arbeitscontainer.reset(ArbeitenContainer::instance());
    }
}

void Student::abschluss_initialisieren()
{
    if(!(_abschlusscontainer.get()))
    {
        _abschlusscontainer.reset(AbschlussarbeitenContainer::instance_abschlussarbeiten());
    }
}

void Student::projekt_initialisieren()
{
    if(!(_projektcontainer.get()))
    {
        _projektcontainer.reset(ProjektarbeitenContainer::instance_projektarbeiten());
    }
}
