#include "datenfacade.h"
#include "SQL/Arbeit.h"
#include "SQL/ArbeitenContainer.h"

Datenfacade::Datenfacade()
{
}

Datenfacade::~Datenfacade()
{
}

Datenfacade & Datenfacade::getfacade()
{
    static Datenfacade * _datenfacade;
    if(_datenfacade == nullptr)
    {
        _datenfacade = new Datenfacade();
    }
    return *_datenfacade;
}

/**********************************************************
 *                                     Administrator                                      *
 **********************************************************/
Administrator & Datenfacade::getadmin()
{
    return Administrator::get_admin();
}

bool Datenfacade::setadmin(const QString &_mail, const QString &_password)
{
    Administrator _admin = Administrator::get_admin();
    if(_admin.set_admin(_mail, _password))
    {
        return true;
    }
    return false;
}

bool Datenfacade::insertdozent(const QString &_vorname, const QString &_nachname,
                                                const QString &_mail, const QString &_password)
{
    Administrator _admin = Administrator::get_admin();
    if(_admin.insert_dozent(_vorname, _nachname, _mail, _password))
    {
        return true;
    }
    return false;
}

bool Datenfacade::updatedozent(const QString &_oldmail, const QString &_vorname, const QString &_nachname,
                                                  const QString &_mail, const QString &_password)
{
    Administrator _admin = Administrator::get_admin();
    if(_admin.update_dozent(_oldmail, _vorname, _nachname, _mail, _password))
    {
        return true;
    }
    return false;
}

bool Datenfacade::deletedozent(const QString &_mail)
{
    Administrator _admin = Administrator::get_admin();
    if(_admin.delete_dozent(_mail))
    {
        return true;
    }
    return false;
}

/**********************************************************
 *                                         Dozent                                           *
 **********************************************************/
bool Datenfacade::insertarbeit(const QString &_art, const QString &_titel, const QString &_stichwort, const QString &_status,
                                           const QString &_studiengang, const QString &_beschreibung, QList<QString> &_bearbeiter, Dozent *_dozent)
{
    Dozent _tempdozent = Dozent();
    if(_tempdozent.insert_arbeit('s', _art, _titel, _stichwort, _status, _studiengang, _beschreibung, _bearbeiter, _dozent))
    {
        return true;
    }
    return false;
}

bool Datenfacade::insertprojekt(const QString &_art, const QString &_titel, const QString &_stichwort, const QString &_status,
                                             const QString &_studiengang, const QString &_beschreibung, QList<QString> &_bearbeiter, Dozent *_dozent,
                                             const QString & _schwerpunkt, const QString & _semester)
{
    Dozent _tempdozent = Dozent();
    if(_tempdozent.insert_arbeit('p', _art, _titel, _stichwort, _status, _studiengang, _beschreibung, _bearbeiter, _dozent, "", QDate::currentDate(), QDate::currentDate(),
                                    _schwerpunkt, _semester))
    {
        return true;
    }
    return false;
}

bool Datenfacade::insertabschluss(const QString &_art, const QString &_titel, const QString &_stichwort, const QString &_status,
                                                    const QString &_studiengang, const QString &_beschreibung, QList<QString> &_bearbeiter, Dozent *_dozent,
                                                    const QString &_firma, const QDate &_von, const QDate &_bis)
{
    Dozent _tempdozent = Dozent();
    if(_tempdozent.insert_arbeit('a', _art, _titel, _stichwort, _status, _studiengang, _beschreibung, _bearbeiter, _dozent, _firma, _von, _bis, "", ""))
    {
        return true;
    }
    return false;
}

bool Datenfacade::updatearbeit(const int &_id, const QString &_art, const QString &_titel, const QString &_stichwort, const QString &_status,
                                              const QString &_studiengang, const QString &_beschreibung, QList<QString> &_bearbeiter, Dozent *_dozent)
{
    Dozent _tempdozent = Dozent();
    if(_tempdozent.update_arbeit('s', _id, _art, _titel, _stichwort, _status, _studiengang, _beschreibung, _bearbeiter, _dozent))
    {
        return true;
    }
    return false;
}

bool Datenfacade::updateprojekt(const int &_id, const QString &_art, const QString &_titel, const QString &_stichwort, const QString &_status,
                                                const QString &_studiengang, const QString &_beschreibung, QList<QString> &_bearbeiter, Dozent *_dozent,
                                                const QString &_schwerpunkt, const QString &_semester)
{
    Dozent _tempdozent = Dozent();
    if(_tempdozent.update_arbeit('p', _id, _art, _titel, _stichwort, _status, _studiengang, _beschreibung, _bearbeiter, _dozent,"", QDate::currentDate(), QDate::currentDate(),
                                               _schwerpunkt, _semester))
    {
        return true;
    }
    return false;
}

bool Datenfacade::updateabschluss(const int &_id, const QString &_art, const QString &_titel, const QString &_stichwort, const QString &_status,
                                                   const QString &_studiengang, const QString &_beschreibung, QList<QString> &_bearbeiter, Dozent *_dozent,
                                                   const QString &_firma, const QDate &_von, const QDate &_bis)
{
    Dozent _tempdozent = Dozent();
    if(_tempdozent.update_arbeit('a', _id, _art, _titel, _stichwort, _status, _studiengang, _beschreibung, _bearbeiter, _dozent, _firma, _von, _bis))
    {
        return true;
    }
    return false;
}

//Löschfunktionen
bool Datenfacade::deletearbeit(const int &_id)
{
    Dozent _dozent = Dozent();
    if(_dozent.delete_arbeit('s', _id))
    {
        return true;
    }
    return false;
}

bool Datenfacade::deleteprojekt(const int &_id)
{
    Dozent _dozent = Dozent();
    if(_dozent.delete_arbeit('p', _id))
    {
        return true;
    }
    return false;
}

bool Datenfacade::deleteabschluss(const int &_id)
{
    Dozent _dozent = Dozent();
    if(_dozent.delete_arbeit('a', _id))
    {
        return true;
    }
    return false;
}

/**********************************************************
 *                                         Student                                          *
 **********************************************************/
QList<Arbeit> search_art(const QString & _gesucht)
{
    Student _student = Student();
    QList<Arbeit> _ergebnisse{};
    QList<int> _zwischenergebnis{};
    //_zwischenergebnis.append(_student.search_art(_gesucht));
    return _ergebnisse;
}
