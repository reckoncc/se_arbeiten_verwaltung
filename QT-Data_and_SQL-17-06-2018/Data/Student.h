#ifndef STUDENT_H
#define STUDENT_H

#include <QString>
#include <QList>
#include <QDate>

#include <memory>

//Vorabdeklaration der Klassen
class Arbeit;
class ArbeitenContainer;
class AbschlussarbeitenContainer;
class ProjektarbeitenContainer;

class Student
{
    public:
        Student();
        ~Student();
        //Suchen
        QList<Arbeit>  search_titel(const QString & _searched);
        QList<Arbeit>  search_art(const QString & _searched);
        QList<Arbeit>  search_schwerpunkt(const QString & _searched);
        QList<Arbeit>  search_bearbeiter(const QString & _searched);
        QList<Arbeit>  search_betreuer(const QString & _searched);
        QList<Arbeit>  search_stichworte(const QList<QString> & _searched);
        QList<Arbeit>  search_datum(const QDate & _von, const QDate & _bis);
    protected:
        //Shared-Ptr auf die verschiedenen Container
        std::shared_ptr<ArbeitenContainer> _arbeitscontainer;
        std::shared_ptr<AbschlussarbeitenContainer> _abschlusscontainer;
        std::shared_ptr<ProjektarbeitenContainer> _projektcontainer;
        // Die zugehörigen Initialisierungsfunktionen
        void arbeiten_initialisieren();
        void abschluss_initialisieren();
        void projekt_initialisieren();
};

#endif
