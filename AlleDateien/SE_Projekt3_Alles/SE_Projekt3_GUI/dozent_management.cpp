#include "dozent_management.h"
#include "ui_dozent_management.h"
#include "login_window.h"
#include "dozenten_dialog.h"
#include "../SE_Projekt3_Alles/SE_Projekt3_Internal/ArbeitenContainer.h"
#include "../SE_Projekt3_Alles/SE_Projekt3_Internal/dozent.h"
#include "dozenten_dialog.h"

#include <QMessageBox>
#include <QDebug>

#include <utility>



Dozent_Management::Dozent_Management(QWidget *parent, QString _loginmail) :
    QMainWindow(parent),
    ui(new Ui::Dozent_Management)
{
    ui->setupUi(this);
    //FileSystemViewer-Einstellen
    _viewer.reset(new QFileSystemWatcher());
    _viewer->addPath("../SE_Projekt3_Alles/SE_Projekt3_Internal/mydb.sqlite");
    //Set _dozentmail
    _dozentmail = _loginmail;
    //Standard-Schriften und Farben
    std::unique_ptr<QFont> _labelstd(new QFont("Arial", 12, QFont::Bold));
    std::unique_ptr<QFont> _menustd(new QFont("Arial", 10));
    std::unique_ptr<QFont> _checkstd(new QFont("Arial", 10));
    std::unique_ptr<QString> _labelsheet(new QString("QLabel {color:white;}"));
    std::unique_ptr<QString> _menusheet(new QString("QMenuBar {color:white;}"));
    std::unique_ptr<QString> _checksheet(new QString("QCheckBox {color:white;}"));

    //Menubars
    ui->menubar->setFont(*_menustd);
    ui->menubar->setStyleSheet(*_menusheet);
    //New Menus
    //Programm
    ui->menuProgramm->setStyleSheet("QMenu {color:white;}");
    ui->menuProgramm->setFont(*_menustd);
    //Benutzer
    ui->menuBenutzer->setStyleSheet("QMenu {color:white;}");
    ui->menuBenutzer->setFont(*_menustd);
    //Arbeit anlegen
    ui->menuArbeit_anlegen->setStyleSheet("QMenu {color:white;}");
    ui->menuArbeit_anlegen->setFont(*_menustd);

    //Arbeit-Buttons
    //Arbeit hinzufügen
    ui->Add_Arbeit->setFixedHeight(_buttonheight);
    ui->Add_Arbeit->setFixedWidth(_buttonwidth);
    ui->Add_Arbeit->setStyleSheet("QPushButton {image: url(:/Icons/Add_Arbeit.png);}");
    ui->Add_Arbeit->setText("");
    //Arbeit ändern
    ui->Update_Arbeit->setFixedHeight(_buttonheight);
    ui->Update_Arbeit->setFixedWidth(_buttonwidth);
    ui->Update_Arbeit->setStyleSheet("QPushButton {image: url(:/Icons/Edit_Arbeit.png);}");
    ui->Update_Arbeit->setText("");
    //Arbeit löschen
    ui->Delete_Arbeit->setFixedHeight(_buttonheight);
    ui->Delete_Arbeit->setFixedWidth(_buttonwidth);
    ui->Delete_Arbeit->setStyleSheet("QPushButton {image: url(:/Icons/Delete_Arbeit.png);}");
    ui->Delete_Arbeit->setText("");

    //Suchleiste
    //Beschreibung
    ui->Suchleisten_Beschreibung->setText("Bitte links ein- oder mehrere\nWerte anwählen!");
    ui->Suchleisten_Beschreibung->setFont(*_labelstd);
    ui->Suchleisten_Beschreibung->setStyleSheet(*_labelsheet);
    ui->Suchleisten_Beschreibung->setAlignment(Qt::AlignHCenter);
    //Eingabefeld
    ui->Suchleiste->setFont(*_labelstd);
    ui->Suchleiste->setStyleSheet("QLineEdit {color:white;}");
    //Such-Button
    ui->SearchButton->setText("");
    ui->SearchButton->setStyleSheet("QPushButton {image: url(:/Icons/magnifying_glasssmall.png);}");
    //Such-Checkboxen
    //Text
    ui->Check_Art->setText("Art");
    ui->Check_Bearbeiter->setText("Bearbeiter");
    ui->Check_Betreuer->setText("Betreuer");
    ui->Check_Datum->setText("Datum");
    ui->Check_Schwerpunkt->setText("Schwerpunkt");
    ui->Check_Titel->setText("Titel");
    //Schriftart
    ui->Check_Art->setFont(*_checkstd);
    ui->Check_Bearbeiter->setFont(*_checkstd);
    ui->Check_Betreuer->setFont(*_checkstd);
    ui->Check_Datum->setFont(*_checkstd);
    ui->Check_Schwerpunkt->setFont(*_checkstd);
    ui->Check_Titel->setFont(*_checkstd);
    //Schriftfarbe
    ui->Check_Art->setStyleSheet(*_checksheet);
    ui->Check_Bearbeiter->setStyleSheet(*_checksheet);
    ui->Check_Betreuer->setStyleSheet(*_checksheet);
    ui->Check_Datum->setStyleSheet(*_checksheet);
    ui->Check_Schwerpunkt->setStyleSheet(*_checksheet);
    ui->Check_Titel->setStyleSheet(*_checksheet);

    //Unteren Tabs
    ui->Arbeitsfenster_Tab->setTabText(0, "Meine Arbeiten");
    ui->Arbeitsfenster_Tab->setTabText(1, "Arbeiten suchen");
    //Obere Tabs
    ui->Toolbar_Tab->setTabText(0, "Arbeit Tools");
    ui->Toolbar_Tab->setTabText(1, "Such-Tools");

    //Meine-Arbeiten-Fenster einrichten und an die DB anbinden
    _sqlptr.reset(new QSqlTableModel(parent = 0, _mydb));
    _sqlptr->setTable("Arbeit");
    _sqlptr->setEditStrategy(QSqlTableModel::OnManualSubmit);
    _sqlptr->select();
    _sqlptr->removeColumn(0);
    ui->Arbeiten_Tabelle->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->Arbeiten_Tabelle->setModel(_sqlptr.get());
    for(auto it = 0; it < _sqlptr->columnCount(); it++)
    {
        ui->Arbeiten_Tabelle->setColumnWidth(it, 129);
    }
    ui->Arbeiten_Tabelle->setColumnWidth(6, 300);
    ui->Arbeiten_Tabelle->setStyleSheet("QTableView {color:white;}");

    //Connects
    Dozent_Management::connect(ui->actionBeenden, SIGNAL(triggered(bool)), this, SLOT(close()));
    Dozent_Management::connect(ui->actionAbmelden, SIGNAL(triggered(bool)), this, SLOT(logout()));
    Dozent_Management::connect(_viewer.get(), SIGNAL(fileChanged(QString)), this, SLOT(update_database()));
    Dozent_Management::connect(ui->Arbeitsfenster_Tab, SIGNAL(currentChanged(int)), ui->Toolbar_Tab, SLOT(setCurrentIndex(int)));
    Dozent_Management::connect(ui->Toolbar_Tab, SIGNAL(currentChanged(int)), ui->Arbeitsfenster_Tab, SLOT(setCurrentIndex(int)));
    Dozent_Management::connect(ui->Add_Arbeit, SIGNAL(clicked(bool)), this, SLOT(open_add_dialog()));


}

Dozent_Management::~Dozent_Management()
{
    delete ui;
}

void Dozent_Management::update_database()
{
    _sqlptr->clear();
    _sqlptr->setTable("Arbeit");
    _sqlptr->setEditStrategy(QSqlTableModel::OnManualSubmit);
    _sqlptr->select();
    _sqlptr->setFilter(QString("Email_Dozent LIKE %1").arg(_dozentmail));
    _sqlptr->removeColumn(0);
    ui->Arbeiten_Tabelle->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->Arbeiten_Tabelle->setModel(_sqlptr.get());
    for(auto it = 0; it < _sqlptr->columnCount(); it++)
    {
        ui->Arbeiten_Tabelle->setColumnWidth(it, 129);
    }
    ui->Arbeiten_Tabelle->setColumnWidth(6, 300);
    ui->Arbeiten_Tabelle->setStyleSheet("QTableView {color:white;}");
}

void Dozent_Management::logout()
{
    int ret;
    std::unique_ptr<QMessageBox> _mb(new QMessageBox());
    std::unique_ptr<Login_Window> _lw(new Login_Window());
    _mb->setText("Möchten Sie sich abmelden?");
    _mb->setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    _mb->setDefaultButton(QMessageBox::Yes);
    ret = _mb->exec();
    switch(ret)
    {
    case QMessageBox::Yes:
        _lw->show();
        _mb.release();
        _lw.release();
        this->close();
        break;
    case QMessageBox::No:
        _lw.release();
        _mb.release();
        return;
    default:
        //Should never be jumped into
        _lw.release();
        _mb.release();
        return;
    }
}

/*void open_add_dialog()
{
    Dozenten_Dialog * _doz = new Dozenten_Dialog();
    _doz->show();
} */
