#ifndef DOZENT_UPDATEN_H
#define DOZENT_UPDATEN_H

#include <QDialog>
#include<memory>
#include<tuple>

namespace Ui {
class Dozent_Updaten;
}

class Dozent_Updaten : public QDialog
{
    Q_OBJECT

public:
    explicit Dozent_Updaten(QWidget *parent = 0);
    ~Dozent_Updaten();
    void set_oldmail(QString oldmail, QString vorname, QString nachname);

private:
    Ui::Dozent_Updaten *ui;
    //Allgemeine Fensterhöhe
    int _height = 30;
    //Hash-Value-Zwischenspeicher
    QString _hashtempsave = "";
    //Alter-Mail-Zwischenspeicher
    QString _oldmail = "";
    QString _vorname="";
    QString _nachname="";

 public slots:
    void set_update_data();
    void create_new_password();
};

#endif // DOZENT_UPDATEN_H
