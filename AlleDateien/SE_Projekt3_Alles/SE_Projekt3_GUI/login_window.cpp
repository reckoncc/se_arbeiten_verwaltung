#include "login_window.h"
#include "ui_login_window.h"
#include "administrator_login.h"
#include "dozent_login.h"
#include "arbeitssuche.h"

Login_Window::Login_Window(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Login_Window)
{
    ui->setupUi(this);

    quint8 button_sizes = 150;
    std::shared_ptr<QString> fontsheet (new QString("QLabel {color : white;}"));
    std::shared_ptr<QFont> labelfont (new QFont("Arial", 12, QFont::Bold));

    //Menubar einstellen
    ui->Menuleiste->setStyleSheet("QMenuBar {color : white;}");
    ui->Programm->setStyleSheet("QMenu {color : white;}");

    //Button-Bilder einstellen
    ui->Admin_Button->setFixedHeight(button_sizes);
    ui->Admin_Button->setFixedWidth(button_sizes);
    ui->Admin_Button->setStyleSheet("QPushButton {image: url(:/Icons/administrator.png);}");
    ui->Admin_Button->setText("");


    ui->Dozent_Button->setFixedHeight(button_sizes);
    ui->Dozent_Button->setFixedWidth(button_sizes);
    ui->Dozent_Button->setStyleSheet("QPushButton {image: url(:/Icons/dozent.png);}");
    ui->Dozent_Button->setText("");

    ui->Gast_Button->setFixedHeight(button_sizes);
    ui->Gast_Button->setFixedWidth(button_sizes);
    ui->Gast_Button->setStyleSheet("QPushButton {image: url(:/Icons/student.png);}");
    ui->Gast_Button->setText("");

    //Button-Labels einstellen
    ui->Admin_Label->setText("Administrator");
    ui->Admin_Label->setStyleSheet(*fontsheet);
    ui->Admin_Label->setFont(*labelfont);
    ui->Admin_Label->setAlignment(Qt::AlignCenter);

    ui->Dozent_Label->setText("Dozent");
    ui->Dozent_Label->setStyleSheet(*fontsheet);
    ui->Dozent_Label->setFont(*labelfont);
    ui->Dozent_Label->setAlignment(Qt::AlignCenter);

    ui->Gast_Label->setText("Gast");
    ui->Gast_Label->setStyleSheet(*fontsheet);
    ui->Gast_Label->setFont(*labelfont);
    ui->Gast_Label->setAlignment(Qt::AlignCenter);

    //Login-Label
    ui->Login_Label->setText("Bitte wählen Sie eine Accountart aus");
    ui->Login_Label->setStyleSheet(*fontsheet);
    ui->Login_Label->setFont(*(new QFont("Arial", 16, QFont::Bold)));
    ui->Login_Label->setAlignment(Qt::AlignCenter);

    //Connects Window-Transitions
    Login_Window::connect(ui->Admin_Button, SIGNAL(clicked()), this, SLOT(open_admin_login()));
    Login_Window::connect(ui->Dozent_Button, SIGNAL(clicked()), this, SLOT(open_dozent_login()));
    Login_Window::connect(ui->Gast_Button, SIGNAL(clicked()), this, SLOT(open_gast_login()));

    //Connects Menubar
    Login_Window::connect(ui->Datei_Beenden, SIGNAL(triggered(bool)), this, SLOT(close()));
}

Login_Window::~Login_Window()
{
    delete ui->Login_Label;
    delete ui;
}

void Login_Window::open_admin_login()
{
    if(!(_adminlogin.get()))
    _adminlogin.reset(new Administrator_Login());
    _adminlogin->show();
    this->close();
}

void Login_Window::open_dozent_login()
{
    if(!(_dozentlogin.get()))
    _dozentlogin.reset(new Dozent_Login());
    _dozentlogin->show();
    this->close();
}

void Login_Window::open_gast_login()
{
    if(!(_gastlogin.get()))
    _gastlogin.reset(new Arbeitssuche());
    _gastlogin->show();
    this->close();
}
