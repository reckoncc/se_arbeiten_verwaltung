#include "arbeitssuche.h"
#include "ui_arbeitssuche.h"
#include "login_window.h"

#include<QMessageBox>

Arbeitssuche::Arbeitssuche(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Arbeitssuche)
{
    ui->setupUi(this);
    std::shared_ptr<QFont> labelstd(new QFont("Arial", 10, QFont::Bold));
    std::unique_ptr<QFont> menustd(new QFont("Arial", 10));
    std::shared_ptr<QString> fontsheet(new QString("QCheckBox {color : white;}"));
    //Tabelle
    ui->Arbeiten_Tabelle->setColumnCount(_tablecolumns);
    for (quint8 it = 0;  it < _tablecolumns; it++)
    {
        ui->Arbeiten_Tabelle->setColumnWidth(it, 131);
    }
    ui->Arbeiten_Tabelle->setHorizontalHeaderItem(0, new QTableWidgetItem("Titel"));
    ui->Arbeiten_Tabelle->setHorizontalHeaderItem(1, new QTableWidgetItem("Art"));
    ui->Arbeiten_Tabelle->setHorizontalHeaderItem(2, new QTableWidgetItem("Schwerpunkt"));
    ui->Arbeiten_Tabelle->setHorizontalHeaderItem(3, new QTableWidgetItem("Betreuer"));
    ui->Arbeiten_Tabelle->setHorizontalHeaderItem(4, new QTableWidgetItem("Bearbeiter"));
    ui->Arbeiten_Tabelle->setHorizontalHeaderItem(5, new QTableWidgetItem("Status"));
    ui->Arbeiten_Tabelle->setFont(*labelstd);

    //Checkboxes Text
    ui->Check_Art->setText("Art");
    ui->Check_Bearbeiter->setText("Bearbeiter");
    ui->Check_Betreuer->setText("Betreuer");
    ui->Check_Datum->setText("Datum");
    ui->Check_Schwerpunkt->setText("Schwerpunkt");
    ui->Check_Titel->setText("Titel");

    //Checkboxes Font
    ui->Check_Art->setFont(*labelstd);
    ui->Check_Bearbeiter->setFont(*labelstd);
    ui->Check_Betreuer->setFont(*labelstd);
    ui->Check_Datum->setFont(*labelstd);
    ui->Check_Schwerpunkt->setFont(*labelstd);
    ui->Check_Titel->setFont(*labelstd);

    //Checkboxes Stylesheet
    ui->Check_Art->setStyleSheet(*fontsheet);
    ui->Check_Bearbeiter->setStyleSheet(*fontsheet);
    ui->Check_Betreuer->setStyleSheet(*fontsheet);
    ui->Check_Datum->setStyleSheet(*fontsheet);
    ui->Check_Schwerpunkt->setStyleSheet(*fontsheet);
    ui->Check_Titel->setStyleSheet(*fontsheet);

    //Textlabel für die Suche
    ui->Suchleisten_Beschreibung->setText("Bitte links einen oder mehrere Werte anwählen.\n");
    ui->Suchleisten_Beschreibung->setFont(*(new QFont("Arial", 12, QFont::Bold)));
    ui->Suchleisten_Beschreibung->setStyleSheet("QLabel {color : white};");
    ui->Suchleisten_Beschreibung->setAlignment(Qt::AlignCenter);

    //Suchleiste
    ui->Suchleiste->setFont(*labelstd);
    ui->Suchleiste->setStyleSheet("QLineEdit {color : white};");

    //Suchbutton-Bild
    ui->SearchButton->setText("");
    ui->SearchButton->setStyleSheet("QPushButton {image: url(:/Icons/magnifying_glasssmall.png);}");

    //Menubars
    ui->menubar->setFont(*menustd);
    ui->menubar->setStyleSheet("QMenuBar {color : white;}");

    ui->menuProgramm->setFont(*menustd);
    ui->menuProgramm->setStyleSheet("QMenu {color : white;}");

    ui->menuBenutzer->setFont(*menustd);
    ui->menuBenutzer->setStyleSheet("QMenu {color : white;}");

    //Connects
    Arbeitssuche::connect(ui->actionAbmelden, SIGNAL(triggered(bool)), this, SLOT(logout()));
    Arbeitssuche::connect(ui->actionBeenden, SIGNAL(triggered(bool)), this, SLOT(close()));
}


Arbeitssuche::~Arbeitssuche()
{
    delete ui->Suchleisten_Beschreibung;
    delete ui;
}

void Arbeitssuche::logout()
{
    int ret;
    std::unique_ptr<Login_Window> _dl(new Login_Window());
    std::unique_ptr<QMessageBox> _mb(new QMessageBox());
    _mb->setText("Möchten Sie sich wirklich abmelden?");
    _mb->setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    _mb->setDefaultButton(QMessageBox::Yes);
    ret = _mb->exec();
    switch(ret)
    {
        case QMessageBox::Yes:
        _dl->show();
        _mb.release();
        _dl.release();
        this->close();
        break;

    case QMessageBox::No:
        _mb.release();
        _dl.release();
        return;

    default:
        //Should never happen
        return;
    }

}
