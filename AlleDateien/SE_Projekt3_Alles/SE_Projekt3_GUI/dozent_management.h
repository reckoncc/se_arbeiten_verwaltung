#ifndef DOZENT_MANAGEMENT_H
#define DOZENT_MANAGEMENT_H

#include <QMainWindow>
#include <QSqlTableModel>
#include <QSqlDatabase>
#include <QFileSystemWatcher>
#include <memory>

namespace Ui {
class Dozent_Management;
}

class Dozent_Management : public QMainWindow
{
    Q_OBJECT

public:
    explicit Dozent_Management(QWidget *parent = 0, QString _loginmail="");
    ~Dozent_Management();

private:
    Ui::Dozent_Management *ui;
    int _buttonheight = 75;
    int _buttonwidth = 75;
    QSqlDatabase _mydb;
    std::unique_ptr<QSqlTableModel> _sqlptr;
    std::unique_ptr<QFileSystemWatcher>_viewer;
    QString _dozentmail;

public slots:
    void update_database();
    void logout();
   // void open_add_dialog();
};

#endif // DOZENT_MANAGEMENT_H
