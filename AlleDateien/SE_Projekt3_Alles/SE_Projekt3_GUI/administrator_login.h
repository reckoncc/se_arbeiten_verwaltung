#ifndef ADMINISTRATOR_LOGIN_H
#define ADMINISTRATOR_LOGIN_H

#include <QMainWindow>
#include <memory>

//Vorweg-Klassendefinitionen
class Administrator_Management;
class Login_Window;

namespace Ui {
class Administrator_Login;
}

/*!
 *\class Administrator_Login
 * \author Nils Birkner
 * Die Klasse Administrator_Login ist eine GUI-Klasse, die den Loginscreen des Administrators beinhaltet.
 * Sie leitet sich von QMainWindow ab und ist auch jedesmal das erste Fenster, das der Benutzer beim starten
 * der Software sieht.
 * In dieser Klasse werden die Email und das Passworts des Administrators eingegeben und an anderer Stelle
 * geprüft. Sollte die Überprüfung erfolgreich sein, wird der Administrator weitergeleitet zum nächsten Fenster.
 * Er kann auch den Login Abbrechen und mittels des Zurück-Buttons zurück zum Login_Window gehen.
 */
class Administrator_Login : public QMainWindow
{
    Q_OBJECT

public:
    /*!
     * \brief Konstruktor: Administrator_Login
     * \param QWidget parent = 0
     */
    explicit Administrator_Login(QWidget *parent = 0);
    /*!
     * \brief Destruktor: ~Administrator_Login
     */
    ~Administrator_Login();

private:
    /*!
     * \brief Die Variable ui ist ein Zeiger auf das gesamte Fenster der Administrator_Login-Klasse.
     */
    Ui::Administrator_Login *ui;
    /*!
     * \brief _am
     *  _am ist ein unique_ptr auf eine Administrator_Management-Klasse.
     */
    std::unique_ptr<Administrator_Management> _am;
    /*!
     * \brief lw
     * _lw ist ein unique_ptr auf eine Login_Window-Klasse.
     */
    std::unique_ptr<Login_Window> _lw;

public slots:
    /*!
     * \brief set_admin_management ist ein Slot um nach drücken des Login-Buttons die Eingaben zu prüfen und zur Administrator_Management-Klasse zu wechseln.
     */
    void set_admin_management();
    /*!
     * \brief set_back ist ein Slot, mit dem zurück zum Loginfenster verwiesen wird.
     */
    void set_back();

};

#endif // ADMINISTRATOR_LOGIN_H
