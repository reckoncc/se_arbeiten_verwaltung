#ifndef ADMINISTRATOR_MANAGEMENT_H
#define ADMINISTRATOR_MANAGEMENT_H

#include <QMainWindow>
#include <QPushButton>
#include <QList>
#include <QSqlTableModel>
#include <QFileSystemWatcher>

class Dozent_Anlegen;
class Dozent_Updaten;

namespace Ui {
class Administrator_Management;
}

class Administrator_Management : public QMainWindow
{
    Q_OBJECT

public:
    explicit Administrator_Management(QWidget *parent = 0);
    ~Administrator_Management();

private:
    Ui::Administrator_Management *ui;
    int _table_columns = 3;
    std::unique_ptr<Dozent_Anlegen> _daptr;
    std::unique_ptr<Dozent_Updaten> _duptr;
    std::unique_ptr<QSqlTableModel> _sqltableptr;
    std::unique_ptr<QFileSystemWatcher> _tablewatch{new QFileSystemWatcher(this)};
    QSqlDatabase mydb;


public slots:
    void dialog_new_dozent();
    void update_db();
    void delete_selected_dozent();
    void dialog_update_dozent();
    void logout();
};

#endif // ADMINISTRATOR_MANAGEMENT_H
