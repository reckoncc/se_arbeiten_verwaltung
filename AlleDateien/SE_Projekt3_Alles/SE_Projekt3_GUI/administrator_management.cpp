#include "administrator_management.h"
#include "ui_administrator_management.h"
#include "../SE_Projekt3_Alles/SE_Projekt3_Internal/administrator.h"
#include "../SE_Projekt3_Alles/SE_Projekt3_Internal/dozent.h"
#include "dozent_anlegen.h"
#include "dozent_updaten.h"
#include "login_window.h"

#include<memory>

#include <QTableWidgetItem>
#include <QMessageBox>

Administrator_Management::Administrator_Management(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Administrator_Management)
{
    ui->setupUi(this);

    //Menü-Leiste einfärben
    ui->Menue_Leiste->setStyleSheet("QMenuBar {color : white;}");
    ui->Programm->setStyleSheet("QMenu {color : white;}");
    ui->Benutzer->setStyleSheet("QMenu {color : white;}");

    //Tabelle einrichten
    //Set Database to read
    _sqltableptr.reset(new QSqlTableModel(parent = 0, mydb));
    _sqltableptr->setTable("Dozent");
    _sqltableptr->setEditStrategy(QSqlTableModel::OnManualSubmit);
    _sqltableptr->select();
    _sqltableptr->removeColumn(3);
    ui->Dozenten_Tabelle->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->Dozenten_Tabelle->setModel(_sqltableptr.get());
    for(auto it = 0; it < _table_columns; it++)
    {
        ui->Dozenten_Tabelle->setColumnWidth(it, 258);
    }
    ui->Dozenten_Tabelle->setStyleSheet("QTableView {color:white;}");


    //Dozenten Hinzufügen Button
    ui->Add_Dozent->setText("");
    ui->Add_Dozent->setStyleSheet("QPushButton {image: url(:/Icons/dozent_Neu.png);}");
    ui->Add_Dozent->setFixedHeight(75);
    ui->Add_Dozent->setFixedWidth(75);

    //Dozenten Löschen Button
    ui->Delete_Dozent->setText("");
    ui->Delete_Dozent->setStyleSheet("QPushButton {image: url(:/Icons/dozent_Loeschen.png);}");
    ui->Delete_Dozent->setFixedHeight(75);
    ui->Delete_Dozent->setFixedWidth(75);

    //Dozent Bearbeiten Button
    ui->Update_Dozent->setText("");
    ui->Update_Dozent->setStyleSheet("QPushButton {image: url(:/Icons/dozent_Bearbeiten.png);}");
    ui->Update_Dozent->setFixedHeight(75);
    ui->Update_Dozent->setFixedWidth(75);

    //Suchleistenlabel und Suchleiste selbst anlegen
    ui->Suchleisten_Beschreibung->setText("Einfache Suche. Mögliche Suchen:\nNachname, Vorname, Email");
    ui->Suchleisten_Beschreibung->setStyleSheet("QLabel {color : white;}");
    ui->Suchleisten_Beschreibung->setFont(*(new QFont("Arial", 12, QFont::Bold)));

    ui->Suchleiste->setStyleSheet("QLineEdit {color : white;}");
    ui->SearchButton->setText("");
    ui->SearchButton->setStyleSheet("QPushButton {image: url(:/Icons/magnifying_glasssmall.png);}");
    ui->SearchButton->setFixedHeight(30);
    ui->SearchButton->setFixedWidth(30);

    //Watcher einstellen
    _tablewatch->addPath("C:/Users/Stard/Documents/QT/SE_Projekt3_Alles/SE_Projekt3_Internal/mydb.sqlite");

    //Connections
    Administrator_Management::connect(ui->Add_Dozent, SIGNAL(clicked()), this, SLOT(dialog_new_dozent()));
    Administrator_Management::connect(_tablewatch.get(), SIGNAL(fileChanged(QString)), this, SLOT(update_db()));
    Administrator_Management::connect(ui->Delete_Dozent, SIGNAL(clicked()), this, SLOT(delete_selected_dozent()));
    Administrator_Management::connect(ui->Update_Dozent, SIGNAL(clicked()), this, SLOT(dialog_update_dozent()));
    Administrator_Management::connect(ui->Abmelden, SIGNAL(triggered(bool)), this, SLOT(logout()));
    Administrator_Management::connect(ui->Beenden, SIGNAL(triggered(bool)), this, SLOT(close()));
}

Administrator_Management::~Administrator_Management()
{
    delete ui;
}

void Administrator_Management::dialog_new_dozent()
{
    _daptr.reset(new Dozent_Anlegen);
    _daptr->show();
}

void Administrator_Management::dialog_update_dozent()
{
    _duptr.reset(new Dozent_Updaten());
    std::unique_ptr<QItemSelectionModel> selection;
    selection.reset(ui->Dozenten_Tabelle->selectionModel());
    QString _oldmail;
    if(selection.get())
    {
        if(selection->hasSelection() && selection->selectedRows().count() == 1)
        {
            std::unique_ptr<Dozent>_doz;
            QModelIndexList row = selection->selectedRows();
            QModelIndex index = row.at(0);
            QVariant var  = index.data();
             _oldmail = var.toString();
             //selection.release(); //Watch!
            _doz.reset(Administrator::get_dozent(_oldmail));
             _duptr->set_oldmail(_doz->mail(), _doz->vorname(), _doz->nachname());
            _doz.reset();
             _duptr->show();
        }
    }
    selection.release();
}

void Administrator_Management::update_db()
{
    _sqltableptr->clear();
    _sqltableptr->setTable("Dozent");
    _sqltableptr->select();
    _sqltableptr->removeColumn(3);
    ui->Dozenten_Tabelle->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->Dozenten_Tabelle->setModel(_sqltableptr.get());
    for(auto it = 0; it < _table_columns; it++)
    {
        ui->Dozenten_Tabelle->setColumnWidth(it, 258);
    }
    ui->Dozenten_Tabelle->setStyleSheet("QTableView {color:white;}");
}

void Administrator_Management::delete_selected_dozent()
{
    std::unique_ptr<QItemSelectionModel> selection;
    selection.reset(ui->Dozenten_Tabelle->selectionModel());
    if(selection.get())
    {
        if(selection->hasSelection() && selection->selectedRows().count() == 1)
        {
            QModelIndexList rows = selection->selectedRows();
            QModelIndex index = rows.at(0);
            QVariant var = index.data();
            Administrator::dozent_loeschen(var.toString());
        }
        selection.release();
    }
}

void Administrator_Management::logout()
{
    int ret;
    std::unique_ptr<QMessageBox> _mb(new QMessageBox());
    std::unique_ptr<Login_Window> _lw(new Login_Window());
    _mb->setText("Möchten Sie sich abmelden?");
    _mb->setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    _mb->setDefaultButton(QMessageBox::No);
    ret = _mb->exec();
    switch(ret)
    {
    case QMessageBox::Yes:
      _lw->show();
      _lw.release();
      this->close();

    case QMessageBox::No:
        return;
    }
}
