#include "dozent_anlegen.h"
#include "ui_dozent_anlegen.h"
#include "../SE_Projekt3_Alles/SE_Projekt3_Internal/check_logins.h"
#include "../SE_Projekt3_Alles/SE_Projekt3_Internal/dozent.h"
#include "../SE_Projekt3_Alles/SE_Projekt3_Internal/administrator.h"

#include<memory>

Dozent_Anlegen::Dozent_Anlegen(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dozent_Anlegen)
{
    std::unique_ptr<QFont> labelstd(new QFont("Arial", 12, QFont::Bold));
    std::unique_ptr<QFont> littlestd(new QFont("Arial", 10));
    std::unique_ptr<QString> fontsheet(new QString("QLabel {color : white;}"));
    std::unique_ptr<QString> buttonsheet(new QString("QPushButton {color : white;}"));
    std::unique_ptr<QString> inputsheet(new QString("QLineEdit {color : white;}"));
    ui->setupUi(this);


    //Begrüßungslabel aktivieren
    ui->Dialog_Text->setText("Einen neuen Dozenten\nanlegen");
    ui->Dialog_Text->setFont(*(new QFont("Arial", 18, QFont::Bold)));
    ui->Dialog_Text->setStyleSheet(*fontsheet);

    //Buttons festlegen
    ui->Abbrechen_Button->setText("Abbrechen");
    ui->Abbrechen_Button->setFont(*littlestd);
    ui->Abbrechen_Button->setStyleSheet(*buttonsheet);

    ui->Uebernehmen_Button->setText("Übernehmen");
    ui->Uebernehmen_Button->setFont(*littlestd);
    ui->Uebernehmen_Button->setStyleSheet(*buttonsheet);

    ui->Passwort_Generieren_Button->setText("Passwort\ngenerieren");
    ui->Passwort_Generieren_Button->setFont(*littlestd);
    ui->Passwort_Generieren_Button->setStyleSheet(*buttonsheet);
    ui->Passwort_Generieren_Button->setFixedHeight(50);
    ui->Passwort_Generieren_Button->setFixedWidth(100);

    //Labels für Eingaben festlegen
    ui->Eingabe_Text_Vorname->setText("Vorname");
    ui->Eingabe_Text_Vorname->setFont(*labelstd);
    ui->Eingabe_Text_Vorname->setStyleSheet(*fontsheet);

    ui->Eingabe_Text_Nachname->setText("Nachname");
    ui->Eingabe_Text_Nachname->setFont(*labelstd);
    ui->Eingabe_Text_Nachname->setStyleSheet(*fontsheet);

    ui->Eingabe_Text_Email->setText("Email");
    ui->Eingabe_Text_Email->setFont(*labelstd);
    ui->Eingabe_Text_Email->setStyleSheet(*fontsheet);

    ui->Eingabe_Text_Kennwort->setText("Kennwort");
    ui->Eingabe_Text_Kennwort->setFont(*labelstd);
    ui->Eingabe_Text_Kennwort->setStyleSheet(*fontsheet);

    //Einabefelder einstellen
    ui->Eingabe_Vorname->setFont(*labelstd);
    ui->Eingabe_Vorname->setStyleSheet(*inputsheet);
    ui->Eingabe_Vorname->setFixedHeight(_height);

    ui->Eingabe_Nachname->setFont(*labelstd);
    ui->Eingabe_Nachname->setStyleSheet(*inputsheet);
    ui->Eingabe_Nachname->setFixedHeight(_height);

    ui->Eingabe_Mail->setFont(*labelstd);
    ui->Eingabe_Mail->setStyleSheet(*inputsheet);
    ui->Eingabe_Mail->setFixedHeight(_height);

    ui->Eingabe_Kennwort->setFont(*labelstd);
    ui->Eingabe_Kennwort->setStyleSheet(*inputsheet);
    ui->Eingabe_Kennwort->setFixedHeight(_height);

    ui->Error_Label->setFont(*(new QFont("Arial", 10, QFont::Bold)));
    ui->Error_Label->setStyleSheet("QLabel { color : red; }");
    ui->Error_Label->setText("Fehler, bitte Eingaben überprüfen!");
    ui->Error_Label->setVisible(false);

    //Connects
    Dozent_Anlegen::connect(ui->Passwort_Generieren_Button, SIGNAL(clicked()), this, SLOT(create_new_password()));
    Dozent_Anlegen::connect(ui->Uebernehmen_Button, SIGNAL(clicked()), this, SLOT(create_new_dozent()));
    Dozent_Anlegen::connect(ui->Abbrechen_Button, SIGNAL(clicked()), this, SLOT(close()));

    //Aufräumen
    labelstd.release();
    littlestd.release();
    fontsheet.release();
    buttonsheet.release();
    inputsheet.release();
}

Dozent_Anlegen::~Dozent_Anlegen()
{
    delete ui->Dialog_Text;
    delete ui->Error_Label;
    delete ui;
}

//Slots
void Dozent_Anlegen::create_new_password()
{
    std::tuple<QString, size_t> tup = Administrator::kennwort_generieren();
    ui->Eingabe_Kennwort->setText(std::get<0>(tup));
    _hashtempsave.append(QString::number(std::get<1>(tup)));
}

void Dozent_Anlegen::create_new_dozent()
{
    ui->Error_Label->setVisible(false);

    Check_Logins _cl = Check_Logins::get_checklogin();
    QString tempvname = ui->Eingabe_Vorname->text();
    QString tempnname = ui->Eingabe_Nachname->text();
    QString tempmail = ui->Eingabe_Mail->text();
    QString tempkennwort = ui->Eingabe_Kennwort->text();
    //Testen, ob alles funktioniert...
    if(!(_cl.check_names(tempvname, tempnname)))
    {
        ui->Error_Label->setText("Fehler bei Eingabe der Namen!");
        ui->Error_Label->setVisible(true);
        return;
    }
    if(!(_cl.check_mail(tempmail)))
    {
        ui->Error_Label->setText("Fehler bei Eingabe der Mail!");
        ui->Error_Label->setVisible(true);
        return;
    }
    if(!(_cl.check_kennwort(tempkennwort)))
    {
        ui->Error_Label->setText("Fehler bei der Kennworteingabe!");
        ui->Error_Label->setVisible(true);
        return;
    }
    //Passwort schauen ob schon gehasht, wenn nicht erst hashen
    if(_hashtempsave == "")
    {
        std::hash<std::string>hs;
        std::basic_string<char>bs = tempkennwort.toStdString();
        _hashtempsave.append(QString::number(hs(bs)));
    }
    //Jetzt wird alles eingefügt
    if(!(Administrator::dozent_anlegen(tempvname, tempnname, tempmail, _hashtempsave)))
    {
        ui->Error_Label->setText("Fehler beim Anlegen!");
        ui->Error_Label->setVisible(true);
        _hashtempsave = "";
        return;
    }
    ui->Error_Label->setVisible(false);
    _hashtempsave = "";
    this->close();
}




