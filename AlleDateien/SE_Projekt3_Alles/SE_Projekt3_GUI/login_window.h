#ifndef LOGIN_WINDOW_H
#define LOGIN_WINDOW_H

#include <QMainWindow>
#include <memory>

class Administrator_Login;
class Dozent_Login;
class Arbeitssuche;

namespace Ui {
class Login_Window;
}

class Login_Window : public QMainWindow
{
    Q_OBJECT

public:
    explicit Login_Window(QWidget *parent = 0);
    ~Login_Window();

private:
    Ui::Login_Window *ui;
    std::unique_ptr<Administrator_Login>_adminlogin;
    std::unique_ptr<Dozent_Login>_dozentlogin;
    std::unique_ptr<Arbeitssuche>_gastlogin;

 public slots:
    void open_admin_login();
    void open_dozent_login();
    void open_gast_login();

};

#endif // LOGIN_WINDOW_H
