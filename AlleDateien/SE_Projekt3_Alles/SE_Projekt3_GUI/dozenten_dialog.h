#ifndef DOZENTEN_DIALOG_H
#define DOZENTEN_DIALOG_H

#include <QDialog>

namespace Ui {
class Dozenten_Dialog;
}

class Dozenten_Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dozenten_Dialog(QWidget *parent = 0);
    ~Dozenten_Dialog();

private:
    Ui::Dozenten_Dialog *ui;
};

#endif // DOZENTEN_DIALOG_H
