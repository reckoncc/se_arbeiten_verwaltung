#ifndef ARBEITSSUCHE_H
#define ARBEITSSUCHE_H

#include <QMainWindow>

namespace Ui {
class Arbeitssuche;
}

class Arbeitssuche : public QMainWindow
{
    Q_OBJECT

public:
    explicit Arbeitssuche(QWidget *parent = 0);
    ~Arbeitssuche();

private:
    Ui::Arbeitssuche *ui;
    quint8 _tablecolumns = 6;

public slots:
    void logout();
};

#endif // ARBEITSSUCHE_H
