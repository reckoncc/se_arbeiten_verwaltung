#ifndef DOZENT_LOGIN_H
#define DOZENT_LOGIN_H

#include <QMainWindow>

class Login_Window;

namespace Ui {
class Dozent_Login;
}

class Dozent_Login : public QMainWindow
{
    Q_OBJECT

public:
    explicit Dozent_Login(QWidget *parent = 0);
    ~Dozent_Login();


private:
    Ui::Dozent_Login *ui;
    Login_Window * lw;
    QString _tempmail;

public slots:
    void window_back();
    void login();
};

#endif // DOZENT_LOGIN_H
