#include "dozenten_dialog.h"
#include "ui_dozenten_dialog.h"

#include <memory>

Dozenten_Dialog::Dozenten_Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dozenten_Dialog)
{
    ui->setupUi(this);

    //Standard-Aussehen festlegen
    std::unique_ptr<QFont> _labelstd(new QFont("Arial", 12, QFont::Bold));
    std::unique_ptr<QString> _labelsheet(new QString("QLabel {color:white;}"));

    //Beschreibung des Dialogs
    ui->beschreibung_label->setText("Bitte füllen Sie alle Eingaben aus.");
    ui->beschreibung_label->setStyleSheet(*_labelsheet);
    ui->beschreibung_label->setFont(*_labelstd);
    ui->beschreibung_label->setAlignment(Qt::AlignCenter);

    //Dialog-Labels
    ui->titel_label->setText("Titel");
    ui->titel_label->setStyleSheet(*_labelsheet);
    ui->titel_label->setFont(*_labelstd);
    ui->titel_label->setAlignment(Qt::AlignCenter);

    ui->erlaeuterung_label->setText("Erläuterung");
    ui->erlaeuterung_label->setStyleSheet(*_labelsheet);
    ui->erlaeuterung_label->setFont(*_labelstd);
    ui->erlaeuterung_label->setAlignment(Qt::AlignCenter);

    ui->stichwort_label->setText("Stichworte");
    ui->stichwort_label->setStyleSheet(*_labelsheet);
    ui->stichwort_label->setFont(*_labelstd);
    ui->stichwort_label->setAlignment(Qt::AlignCenter);

    ui->Status_label->setText("Status");
    ui->Status_label->setStyleSheet(*_labelsheet);
    ui->Status_label->setFont(*_labelstd);
    ui->Status_label->setAlignment(Qt::AlignCenter);

    ui->art_label->setText("Arbeitsart");
    ui->art_label->setStyleSheet(*_labelsheet);
    ui->art_label->setFont(*_labelstd);
    ui->art_label->setAlignment(Qt::AlignCenter);

    ui->studiengang_label->setText("Studiengang");
    ui->studiengang_label->setStyleSheet(*_labelsheet);
    ui->studiengang_label->setFont(*_labelstd);
    ui->studiengang_label->setAlignment(Qt::AlignCenter);

    ui->betreuer_label->setText("Betreuer");
    ui->betreuer_label->setStyleSheet(*_labelsheet);
    ui->betreuer_label->setFont(*_labelstd);
    ui->betreuer_label->setAlignment(Qt::AlignCenter);

    ui->bearbeiter_label->setText("Bearbeiter");
    ui->bearbeiter_label->setStyleSheet(*_labelsheet);
    ui->bearbeiter_label->setFont(*_labelstd);
    ui->bearbeiter_label->setAlignment(Qt::AlignCenter);

}

Dozenten_Dialog::~Dozenten_Dialog()
{
    delete ui;
}
