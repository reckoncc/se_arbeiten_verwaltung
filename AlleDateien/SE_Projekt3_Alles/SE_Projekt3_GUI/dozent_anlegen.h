#ifndef DOZENT_ANLEGEN_H
#define DOZENT_ANLEGEN_H

#include <QDialog>
#include <QList>
#include <tuple>

class Dozent;
class Administrator;

namespace Ui {
class Dozent_Anlegen;
}

class Dozent_Anlegen : public QDialog
{
    Q_OBJECT

public:
    explicit Dozent_Anlegen(QWidget *parent = 0);
    ~Dozent_Anlegen();
    bool check_names(const QString &vorname, const QString &nachname);
    bool check_mail(const QString &mail);
    bool check_kennwort(const QString &kennwort);



private:
    //Standard-UI
    Ui::Dozent_Anlegen *ui;
    //Allgemeine Fensterhöhe
    int _height = 30;
    //Hash-Value-Zwischenspeicher
    QString _hashtempsave = "";


public slots:
       void create_new_password();
       void create_new_dozent();

};

#endif // DOZENT_ANLEGEN_H
