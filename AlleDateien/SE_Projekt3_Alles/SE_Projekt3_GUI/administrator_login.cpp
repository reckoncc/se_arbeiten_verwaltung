#include "administrator_login.h"
#include "ui_administrator_login.h"
#include "administrator_management.h"
#include "login_window.h"
#include "../SE_Projekt3_Alles/SE_Projekt3_Internal/administrator.h"
#include "../SE_Projekt3_Alles/SE_Projekt3_Internal/check_logins.h"

Administrator_Login::Administrator_Login(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Administrator_Login)
{
    ui->setupUi(this);
    //Standardfont and Standard labelsheet
    std::shared_ptr<QFont> labelstd (new QFont("Arial", 12, QFont::Bold));
    std::shared_ptr<QString> labelsheet (new QString("QLabel {color : white;}"));
    //Menüleiste weiß färben
    ui->Menue_Leiste->setStyleSheet("QMenuBar {color : white;}");
    ui->menuProgramm->setStyleSheet("QMenu {color : white;}");

    //Zurück-Button
    ui->Zurueck_Button->setText("");
    ui->Zurueck_Button->setFixedHeight(100);
    ui->Zurueck_Button->setFixedWidth(100);
    ui->Zurueck_Button->setStyleSheet("QPushButton {image: url(:/Icons/Arrow_Back.png);}");

    //Textlabel festsetzen
    ui->Text_Label->setFont(*(new QFont("Arial", 18, QFont::Bold)));
    ui->Text_Label ->setText("Bitte melden Sie sich an.");
    ui->Text_Label->setAlignment(Qt::AlignCenter);
    ui->Text_Label->setStyleSheet(*labelsheet);

    //Email und Passwortfeld einstellen
    ui->Email_Label->setFont(*(labelstd.get()));
    ui->Email_Label->setStyleSheet(*labelsheet);
    ui->Email_Label->setFixedHeight(20);
    ui->Email_Label->setText("Email");

    ui->Email_Eingabe->setFont(*(labelstd.get()));
    ui->Email_Eingabe->setStyleSheet("QLineEdit {color : white;}");
    ui->Email_Eingabe->setFixedHeight(25);
    ui->Email_Eingabe->setMaxLength(50);

    ui->Passwort_Label->setFont(*(labelstd.get()));
    ui->Passwort_Label->setStyleSheet(*labelsheet);
    ui->Passwort_Label->setFixedHeight(20);
    ui->Passwort_Label->setText("Passwort");

    ui->Passwort_Eingabe->setFont(*(labelstd.get()));
    ui->Passwort_Eingabe->setStyleSheet("QLineEdit {color : white;}");
    ui->Passwort_Eingabe->setFixedHeight(22);
    ui->Passwort_Eingabe->setMaxLength(50);
    ui->Passwort_Eingabe->setEchoMode(QLineEdit::Password);

    //Clear und Login-Button
    ui->Clear_Button->setFont(*(labelstd.get()));
    ui->Clear_Button->setStyleSheet("QPushButton {color : white;}");
    ui->Clear_Button->setFixedHeight(75);
    ui->Clear_Button->setFixedWidth(125);
    ui->Clear_Button->setText("Leeren");

    ui->Login_Button->setFont(*(labelstd.get()));
    ui->Login_Button->setStyleSheet("QPushButton {color : white;}");
    ui->Login_Button->setFixedHeight(75);
    ui->Login_Button->setFixedWidth(125);
    ui->Login_Button->setText("Login");

    //Dozentenbild einfügen
    ui->Admin_Bild->setText("");
    ui->Admin_Bild->setStyleSheet("QLabel {image: url(:/Icons/administrator.png);}");
    ui->Admin_Bild->setFixedHeight(150);
    ui->Admin_Bild->setFixedWidth(150);

    //Dozentenbild beschriften
    ui->Admin_Name_Label->setText("Administrator");
    ui->Admin_Name_Label->setFont(*(labelstd.get()));
    ui->Admin_Name_Label->setStyleSheet(*labelsheet);
    ui->Admin_Name_Label->setAlignment(Qt::AlignCenter);

    //Error-Label beschriften
    ui->Error_Label->setVisible(false);
    ui->Error_Label->setStyleSheet("QLabel {color:red;}");
    ui->Error_Label->setFont(*labelstd);
    ui->Error_Label->setAlignment(Qt::AlignCenter);

    //Connects
    Administrator_Login::connect(ui->Zurueck_Button, SIGNAL(clicked()), this, SLOT(set_back()));
    Administrator_Login::connect(ui->Login_Button, SIGNAL(clicked()), this, SLOT(set_admin_management()));
    Administrator_Login::connect(ui->actionBeenden, SIGNAL(triggered(bool)), SLOT(close()));
}

Administrator_Login::~Administrator_Login()
{
    delete ui->Text_Label;
    delete ui;
}

void Administrator_Login::set_admin_management()
{
    Check_Logins & _cl = Check_Logins::get_checklogin();
    bool _login = false;
    if(_login = _cl.check_admin_login(ui->Email_Eingabe->text(), ui->Passwort_Eingabe->text()))   //Administrator::check_login(ui->Email_Eingabe->text(), ui->Passwort_Eingabe->text())
    {
        _am.reset(new Administrator_Management());
        _am->show();
        this->close();
    }
    else
    {
        ui->Error_Label->setVisible(true);
        ui->Error_Label->setText("Fehler bei der Eingabe der Daten!");
        return;
    }
}

void Administrator_Login::set_back()
{
    _lw.reset(new Login_Window());
    _lw->show();
    this->close();
}
