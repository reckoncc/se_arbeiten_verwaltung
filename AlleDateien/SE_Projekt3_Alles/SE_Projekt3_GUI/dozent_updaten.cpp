#include "dozent_updaten.h"
#include "ui_dozent_updaten.h"
#include "../SE_Projekt3_Alles/SE_Projekt3_Internal/check_logins.h"
#include "../SE_Projekt3_Alles/SE_Projekt3_Internal/administrator.h"

#include <QTableWidgetItem>
#include<QSqlDatabase>

Dozent_Updaten::Dozent_Updaten(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dozent_Updaten)
{
    std::unique_ptr<QFont> labelstd(new QFont("Arial", 12, QFont::Bold));
    std::unique_ptr<QFont> littlestd(new QFont("Arial", 10));
    std::unique_ptr<QString> fontsheet(new QString("QLabel {color : white;}"));
    std::unique_ptr<QString> buttonsheet(new QString("QPushButton {color : white;}"));
    std::unique_ptr<QString> inputsheet(new QString("QLineEdit {color : white;}"));
    ui->setupUi(this);


    //Begrüßungslabel aktivieren
    ui->Dialog_Text_3->setText("Einen Dozenten\nupdaten");
    ui->Dialog_Text_3->setFont(*(new QFont("Arial", 18, QFont::Bold)));
    ui->Dialog_Text_3->setStyleSheet(*fontsheet);

    //Buttons festlegen
    ui->Abbrechen_Button_3->setText("Abbrechen");
    ui->Abbrechen_Button_3->setFont(*littlestd);
    ui->Abbrechen_Button_3->setStyleSheet(*buttonsheet);

    ui->Uebernehmen_Button_3->setText("Übernehmen");
    ui->Uebernehmen_Button_3->setFont(*littlestd);
    ui->Uebernehmen_Button_3->setStyleSheet(*buttonsheet);

    ui->Passwort_Generieren_Button_3->setText("Passwort\ngenerieren");
    ui->Passwort_Generieren_Button_3->setFont(*littlestd);
    ui->Passwort_Generieren_Button_3->setStyleSheet(*buttonsheet);
    ui->Passwort_Generieren_Button_3->setFixedWidth(100);

    //Labels für Eingaben festlegen
    ui->Eingabe_Text_Vorname_3->setText("Vorname");
    ui->Eingabe_Text_Vorname_3->setFont(*labelstd);
    ui->Eingabe_Text_Vorname_3->setStyleSheet(*fontsheet);

    ui->Eingabe_Text_Nachname_3->setText("Nachname");
    ui->Eingabe_Text_Nachname_3->setFont(*labelstd);
    ui->Eingabe_Text_Nachname_3->setStyleSheet(*fontsheet);

    ui->Eingabe_Text_Email_3->setText("Email");
    ui->Eingabe_Text_Email_3->setFont(*labelstd);
    ui->Eingabe_Text_Email_3->setStyleSheet(*fontsheet);

    ui->Eingabe_Text_Kennwort_3->setText("Kennwort");
    ui->Eingabe_Text_Kennwort_3->setFont(*labelstd);
    ui->Eingabe_Text_Kennwort_3->setStyleSheet(*fontsheet);

    //Einabefelder einstellen
    ui->Eingabe_Vorname_3->setFont(*labelstd);
    ui->Eingabe_Vorname_3->setStyleSheet(*inputsheet);
    ui->Eingabe_Vorname_3->setFixedHeight(_height);

    ui->Eingabe_Nachname_3->setFont(*labelstd);
    ui->Eingabe_Nachname_3->setStyleSheet(*inputsheet);
    ui->Eingabe_Nachname_3->setFixedHeight(_height);

    ui->Eingabe_Mail_3->setFont(*labelstd);
    ui->Eingabe_Mail_3->setStyleSheet(*inputsheet);
    ui->Eingabe_Mail_3->setFixedHeight(_height);

    ui->Eingabe_Kennwort_3->setFont(*labelstd);
    ui->Eingabe_Kennwort_3->setStyleSheet(*inputsheet);
    ui->Eingabe_Kennwort_3->setFixedHeight(_height);

    ui->Error_Label->setFont(*(new QFont("Arial", 10, QFont::Bold)));
    ui->Error_Label->setStyleSheet("QLabel { color : red; }");
    ui->Error_Label->setText("Fehler, bitte Eingaben überprüfen!");
    ui->Error_Label->setVisible(false);

    //Connects
    Dozent_Updaten::connect(ui->Passwort_Generieren_Button_3, SIGNAL(clicked()), this, SLOT(create_new_password()));
    Dozent_Updaten::connect(ui->Uebernehmen_Button_3, SIGNAL(clicked()), this, SLOT(set_update_data()));
    Dozent_Updaten::connect(ui->Abbrechen_Button_3, SIGNAL(clicked()), this, SLOT(close()));

    //Aufräumen
    labelstd.release();
    littlestd.release();
    fontsheet.release();
    buttonsheet.release();
    inputsheet.release();
}

Dozent_Updaten::~Dozent_Updaten()
{
    delete ui->Dialog_Text_3;
    delete ui->Error_Label;
    delete ui;
}

void Dozent_Updaten::set_oldmail(QString oldmail,QString vorname, QString nachname)
{
    this->_oldmail = oldmail;
    this->_vorname = vorname;
    this->_nachname = nachname;
    ui->Eingabe_Vorname_3->setText(_vorname);
    ui->Eingabe_Nachname_3->setText(_nachname);
    ui->Eingabe_Mail_3->setText(_oldmail);
}

void Dozent_Updaten::set_update_data()
{
    Check_Logins _cl = Check_Logins::get_checklogin();
    bool _passwordflag = true;
    ui->Error_Label->setVisible(false);
    QString vorname = ui->Eingabe_Vorname_3->text();
    QString nachname = ui->Eingabe_Nachname_3->text();
    QString mail = ui->Eingabe_Mail_3->text();
    QString kennwort = ui->Eingabe_Kennwort_3->text();
    if(!(_cl.check_names(vorname, nachname)))
    {
        ui->Error_Label->setText("Fehler bei Eingabe des Namens!");
        ui->Error_Label->setVisible(true);
        return;
    }
    if(!(_cl.check_mail(mail)))
    {
        ui->Error_Label->setText("Fehler bei der Eingabe der Mail!");
        ui->Error_Label->setVisible(true);
        return;
    }
    if(!(_cl.check_kennwort(kennwort)))
    {
        _passwordflag = false;
    }
    if(_hashtempsave == "")
    {
        std::hash<std::string> hs;
        std::basic_string<char> bs = kennwort.toStdString();
         _hashtempsave.append(QString::number(hs(bs)));
    }
    if(_passwordflag == false)
    {
        Administrator::dozent_updaten(_oldmail, vorname, nachname, mail, "");
    }
    else
    {
        Administrator::dozent_updaten(_oldmail, vorname, nachname, mail, _hashtempsave);
    }
    _hashtempsave = "";
    this->close();

}

void Dozent_Updaten::create_new_password()
{
    std::tuple<QString, size_t> tup = Administrator::kennwort_generieren();
    ui->Eingabe_Kennwort_3->setText(std::get<0>(tup));
    _hashtempsave.append(QString::number(std::get<1>(tup)));
}
