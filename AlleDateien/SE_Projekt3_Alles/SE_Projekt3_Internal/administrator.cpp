#include <QRandomGenerator64>
#include <QDateTime>
#include <QSqlQuery>
#include <QDebug>

#include "administrator.h"
#include"../SE_Projekt3_Alles/SE_Projekt3_GUI/dozent_anlegen.h"
#include "dozentenContainer.h"
#include "dozent.h"

/*!
 * \brief Administrator::Administrator
 * \param email
 * \param id
 * \param kennwort
 * Konstruktor der Administratoren-Klasse
 */
Administrator::Administrator(const QString & email, const QString & kennwort)
    : _email(email), _kennwort(kennwort)
{
}

/*!
 * \brief Administrator::~Administrator
 * Destruktor hat nichts speziell zu löschen
 */
Administrator::~Administrator()
{
}
/*!
 * \brief Administrator::dozent_anlegen
 * \param vorname
 * \param nachname
 * \param email
 * \param kennwort
 * \return boolean
 */
bool Administrator::dozent_anlegen(const QString &vorname, const QString &nachname,
                                                      const QString &mail, const QString &kennwort)
{
    std::unique_ptr<DozentenContainer>_dc;
    _dc.reset(DozentenContainer::dozentenContainer());
    if(_dc->insert(vorname, nachname, mail, kennwort)){_dc.release(); return true;}
    else{_dc.release(); return false;}
}

bool Administrator::dozent_updaten(const QString & oldmail, const QString &vorname, const QString &nachname,
                                                      const QString &mail, const QString &kennwort = "")
{
    std::unique_ptr<DozentenContainer>_dc;
    _dc.reset(DozentenContainer::dozentenContainer());
    if(kennwort =="")
    {
        _dc->update(oldmail, mail, nachname, vorname, "");
    }
    else
    {
        _dc->update(oldmail, mail, nachname, vorname, kennwort);
    }
    _dc.release();
    return true;
}

/*!
 * \brief Administrator::dozent_loeschen
 * \param zuloeschen
 * \return boolean
 */
bool Administrator::dozent_loeschen(const QString & mail)
{
    std::unique_ptr<DozentenContainer>_dc;
    _dc.reset(DozentenContainer::dozentenContainer());
    if(_dc->remove(mail)){_dc.release(); return true;}
    else{_dc.release(); return false;}
}

/*!
 * \brief Administrator::kennwort_generieren
 * \return QString
 */
std::tuple<QString, size_t> Administrator::kennwort_generieren()
{
    /*!
     * \brief _charflag
     * Ein Boolean-Flag, das anzeigt ob ein Character bereits gewählt wurde.
     */
    bool _charflag = false;
    /*!
     * \brief _numberflag
     */
    bool _numberflag = false;
    /*!
     * \brief _extraflag
     */
    bool _extraflag = false;
    /*!
     * \details In diesem Abschnitt wird ein zufälliger Seed erstellt, der für einen \var QRandomGenerator
     * als Konstruktor-Parameter verwendet wird. Es wird zudem ein Hashwert erstellt, der zusätzlich mittels
     * std::tuple als Rückgabewert zurückgegeben wird.
     */
    qsrand(QDateTime::currentMSecsSinceEpoch() / 1000);
    quint32 _randseed = qrand();
    QRandomGenerator * gen = new QRandomGenerator(_randseed);
    std::hash<std::string>  _hashpass;
    QString _newpass;
    quint32 _max = 10;
    quint32 _rand;
    for(quint32 it = 0; it < _max; it++) //ASCII 33 - 90 & 97 - 126
    {
        _rand = gen->bounded(33,127);
         /*
          * Sollten wir außerhalb es gewünschten ASCII-Zeichensatzes fallen, müssen wir nochmal würfeln.
          * Ansonsten muss noch unterschieden werden, ob Sonderzeichen, Character oder Zahl.
          */
        if (_rand > 90 && _rand < 97){it--; continue;}
        _newpass.append(static_cast<QChar>(_rand));
        if ((_rand > 32 && _rand < 48) || (_rand > 57 && _rand < 65) || (_rand > 90 && _rand < 128))
        {
            _extraflag = true;
        }
        if((_rand > 64 && _rand < 91) || (_rand > 96 && _rand < 123))
        {
            _charflag = true;
        }
        if(_rand > 47 && _rand < 58)
        {
            _numberflag = true;
        }
        if(it == _max -1)
        {
            if(_charflag == true && _numberflag == true && _extraflag == true)
            {
                std::basic_string<char> _bs = _newpass.toStdString();
                size_t _strhash = _hashpass(_bs);
                std::tuple<QString, size_t> tup(_newpass, _strhash);
                return tup;
            }
            else
            {
                it = 0;
                _newpass.clear();
            }
        }
    }
    std::tuple<QString, size_t>tup("", 0);
    return tup;
}

void Administrator::create_administrator(const QString &email, const QString &kennwort)
{
    if(!(_admin.get()))
    {
        QSqlQuery query;
        query.prepare("SELECT * FROM Admin WHERE Email_Admin = ?");
        query.addBindValue(email);
        std::unique_ptr<Administrator> admin;
        if(query.exec())
        {
            query.first();
            QString mail = query.value(0).toString();
            if (mail == "")
            {
                std::hash<std::string>hs;
                std::basic_string<char>bs = kennwort.toStdString();
                QString _hashed = QString::number(hs(bs));
                admin.reset(new Administrator(email, _hashed));
                _admin.reset(admin.get());
                QSqlQuery query2;
                query2.prepare("INSERT INTO Admin(Email_Admin, Kennwort) VALUES(:mail, :kennwort)");
                query2.bindValue(":mail", email);
                query2.bindValue(":kennwort", _hashed);
                if(query2.exec())
                {
                    //Success
                }
                else
                {
                    qDebug() << "Error!";
                }

            }
        }
        else
        {
            std::hash<std::string>hs;
            std::basic_string<char>bs = kennwort.toStdString();
            QString _hashed = QString::number(hs(bs));
            admin.reset(new Administrator(email, _hashed));
            _admin.reset(admin.get());
            QSqlQuery query2;
            query2.prepare("INSERT INTO Admin(Email_Admin, Kennwort) VALUES(:mail, :kennwort)");
            query2.bindValue(":mail", email);
            query2.bindValue(":kennwort", _hashed);
            if(query2.exec())
            {
                //Success
            }
            else
            {
                qDebug() << "Error!";
            }
        }
        admin.release();
        return;
    }
    else
    {
        return;
    }
}

Dozent * Administrator::get_dozent(const QString &mail)
{
    QString vorname, nachname,kennwort;
    std::unique_ptr<Dozent> _doz;
    std::unique_ptr<DozentenContainer> _dc;
    _dc.reset(DozentenContainer::dozentenContainer());
    _doz.reset(_dc->getDozent(mail));
    vorname = _doz->vorname();
    nachname = _doz->nachname();
    kennwort = _doz->kennwort();
    _dc.release();
    _doz.release();
    return new Dozent(vorname, nachname, mail, kennwort);
}
