#ifndef DOZENT_H
#define DOZENT_H

#include <memory>
#include <QList>
#include <QDate>

#include "gast.h"
#include "Datum.h"

class Arbeit;
class ArbeitenContainer;

/*!
 * \brief Die Dozent Klasse
 * \author Nils Birkner
 * Die Klasse Dozent repräsentiert einen Dozenten, der in der Lage
 * ist verschiedene Arbeiten zu erstellen, zu verwalten und zu löschen.
 * Er kann zudem nach verschiedenen Arbeiten suchen.
 */
class Dozent : public Gast
{
private:
    /*!
     * \brief _vorname
     * Diese Variable speichert den Vornamen des Dozenten-Objekts.
     */
    QString _vorname="";
    /*!
     * \brief _nachname
     * Diese Variable speichert den Nachnamen des Dozenten-Objekts.
     */
    QString _nachname="";
    /*!
     * \brief _mail
     * Diese Variable speichert die Email des Dozenten-Objekts.
     */
    QString _mail="";
    /*!
     * \brief _kennwort
     * Diese Variable speichert das Kennwort des Dozenten in einer
     * Hash-Form.
     */
    QString _kennwort;
public:
    /*!
     * \brief Dozent
     * \param vorname
     * \param nachname
     * \param mail
     * \param kennwort
     * Der Konstruktor von Dozent erhält mehrere Parameter die zwingend sind,
     * um ihn gültig einzutragen.
     */
    Dozent(const QString & vorname,const QString & nachname, const QString & mail,
               const QString & kennwort);

    /*!
     * \brief Dozent
     * \param vorname
     * \param nachname
     * \param mail
     * \param kennwort
     * Der Verschiebekonstruktor von Dozent erhält mehrere R-Values um somit ggf ein temporäres
     * Dozentenobjekt direkt verschieben zu können statt zu kopieren.
     */
    Dozent(QString && vorname, QString && nachname, QString && mail, QString && kennwort);

    Dozent() = delete;
    void operator=(const Dozent &) = delete;
    void operator=(Dozent&&) = delete;
    /*!
     * \brief ~Dozent
     * Der Destruktor ist leer, da keine weiteren Objekte gelöscht werden müssen.
     */
    ~Dozent();
    /*!
     * \brief Funktion: arbeit_bearbeiten
     * \param zubearbeiten
     * Die Funktion arbeit_bearbeiten ermöglicht es einem Dozenten, eine beliebige Arbeit,
     * die er selbst angelegt hat zu bearbeiten und verschiedene Werte zu ändern.
     */
    void arbeit_bearbeiten(Arbeit & zubearbeiten);
    /*!
     * \brief Funktion: arbeit_eintragen
     * \param titel
     * \param betreuer
     * \param status
     * \param erlaeuterung
     * \param stichworte
     * \return boolean
     * Die Funktion arbeit_eintragen ermöglicht es dem Dozenten eine neue Arbeit anzulegen
     * um diese später in einem Arbeitscontainer abzulegen.
     */
    bool arbeit_eintragen(const QString &schwerpunkt, const QString & titel, const QString &art, Dozent & betreuer, const QString & status,
                                      const QString & erlaeuterung, const QString & stichworte, QList<QString> & bearbeiter);
    /*!
     * \brief Funktion: arbeit_loeschen
     * \param arbeit
     * \return boolean
     * Die Funktion arbeit_loeschen löscht eine Arbeit,
     * die fälschlicherweise angelegt wurde oder abgebrochen wurde.
     */
    bool arbeit_loeschen(Arbeit & arbeit);

    //Inline-Getter
    QString &vorname(){return this->_vorname;}
    QString &nachname(){return this->_nachname;}
    QString &mail(){return this->_mail;}
    QString &kennwort(){return this->_kennwort;}
};

#endif // DOZENT_H
