#include "suche_arbeiten.h"
#include "Arbeit.h"

#include <QDebug>

//Leerer Konstruktor
Suche_Arbeiten::Suche_Arbeiten()
{
}

//Leerer Destruktor
Suche_Arbeiten::~Suche_Arbeiten()
{
}

Suche_Arbeiten& Suche_Arbeiten::get_arbeitssuche()
{
    static Suche_Arbeiten s = Suche_Arbeiten();
    return s;
}

//Alle returns etc nur um Compilerfehler zu vermeiden!
QList<Arbeit>& Suche_Arbeiten::suche_titel(const QString &titel)
{
    QList<Arbeit>_ql =QList<Arbeit>();
    return _ql;

}

QList<Arbeit>& Suche_Arbeiten::suche_art(const QString &art)
{
    QList<Arbeit>_ql =QList<Arbeit>();
    return _ql;
}

QList<Arbeit>& Suche_Arbeiten::suche_bearbeiter(const QString &bearbeiter)
{
    QList<Arbeit>_ql =QList<Arbeit>();
    return _ql;
}

QList<Arbeit>& Suche_Arbeiten::suche_betreuer(const QString &betreuer)
{
    QList<Arbeit>_ql =QList<Arbeit>();
    return _ql;
}

QList<Arbeit>& Suche_Arbeiten::suche_schwerpunkt(const QString &schwerpunkt)
{
    QList<Arbeit>_ql =QList<Arbeit>();
    return _ql;
}

QList<Arbeit>& Suche_Arbeiten::suche_status(const QString &status)
{
    QList<Arbeit>_ql =QList<Arbeit>();
    return _ql;
}

QList<Arbeit>& Suche_Arbeiten::suche_stichworte(QList<QString> &liste)
{
    QList<Arbeit>_ql =QList<Arbeit>();
    return _ql;
}

void Suche_Arbeiten::print_results(QList<Arbeit> &arbeiten)
{
    for(auto it : arbeiten)
    {
        qDebug() << "Titel: " << it.titel() << "\nArt: " << it.art()
               << "\nBearbeiter: " << it.bearbeiter();
    }
    return;
}
