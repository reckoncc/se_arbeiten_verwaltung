#include <QCoreApplication>
#include <QDebug>
#include <QtSql>


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    qDebug()<<"start";
    QSqlDatabase mydb;
    mydb = QSqlDatabase::addDatabase("QSQLITE");
    mydb.setDatabaseName("C:\\Users\\Stard\\Documents\\QT\\SE_Projekt3_Internal");

    if (! mydb.open()) {
        qDebug()<<"Problem by opening the database.";
    }

    QString q1 = "CREATE TABLE Admin ("
                 "Email_Admin varchar(50) primary key not null,"
                 "Kennwort varchar(50) not null);";

    QString q2 = "CREATE TABLE Dozent ("
                "Email_Dozent varchar(50) primary key not null,"
                "Vorname varchar(50) not null,"
                "Nachname varchar(50) not null,"
                "Kennwort varchar(50) not null);";

    QString q3 = "CREATE TABLE Bearbeiter ("
                 "Benutzer_id int primary key,"
                 "Name varchar(50));";

    QString q4 = "CREATE TABLE Arbeit ("
                                        "Arbeit_id int primary key not null,"
                                        "Email_Dozent varchar(50) references Dozent not null,"
                                        "Studiengang varchar(50) not null,"
                                        "Titel varchar(100) not null,"
                                        "Art varchar(20) not null,"
                                        "Stichwortliste varchar(150) not null,"
                                        "Status varchar(50) not null,"
                                        "Erlaeuterung varchar(100));";

    QString q5 = "CREATE TABLE Abschlussarbeit ("
                    "Arbeit_id int primary key references Arbeit not null,"
                    "Firma varchar(50),"
                    " bis date not null,"
                    " von date not null );";

    QString q6 = "CREATE TABLE Projektarbeit ("
                " Arbeit_id int primary key references Arbeit not null,"
                " Semester varchar(20) not null,"
                " Schwerpunkt varchar(10) not null);";

    QString q7 = "CREATE TABLE bearbeitung("
                "Benutzer_id int,"
                "Arbeit_id int,"
                "foreign key (Arbeit_id) references Arbeit,"
                "foreign key (Benutzer_id) references Bearbeiter,"
                "primary key(Benutzer_id,Arbeit_id));";

    QList<QString> list;
    list.append(q1);
    list.append(q2);
    list.append(q3);
    list.append(q4);
    list.append(q5);
    list.append(q6);
    list.append(q7);

    QString s;
    foreach (s , list) {
        QSqlQuery query;
        if (! query.exec(s)) {
            qDebug()<<"Error by creating table.";
            qDebug()<< s;
        }
    }



    qDebug()<<"end";

    return a.exec();
}

