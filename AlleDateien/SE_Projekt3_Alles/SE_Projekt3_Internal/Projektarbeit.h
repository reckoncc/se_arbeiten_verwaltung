#ifndef PROJEKTARBEIT_H
#define PROJEKTARBEIT_H

#include "Arbeit.h"

class Dozent;

class Projektarbeit : public Arbeit
{
public:
    Projektarbeit(QString art, QString titel, QString stwort,QString stat, QString stg, QString erl, QList<QString> bearbeiter,Dozent * doz , QString punkt, QString s);

    ~Projektarbeit();

    QString schwerpunkt(){return _schwerpunkt;}
    void set_schwerpunkt(QString s);

    QString semester(){return _semester;}
    void set_semester(QString se);



private:
    QString _schwerpunkt;
    QString _semester;
};

#endif // PROJEKTARBEIT_H
