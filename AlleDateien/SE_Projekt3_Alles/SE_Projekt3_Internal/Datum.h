#ifndef DATUM_H
#define DATUM_H

#include <QString>

class Datum {
private:
     int _jahr, _monat, _tag;

public:
    /* Constructor */
    Datum( int jahr, int monat, int tag);

    /* Getters */
    int jahr();
    int monat();
    int tag() ;

    /* Setters */
    void set_jahr(const int jahr);
    void set_monat(const int monat);
    void set_tag(const int tag);

    /* Datum-ToString */
    static QString date_to_string(Datum &date);

    /* Operator Overloading */

    inline bool operator== (Datum& rhs) {
        return (
            this->jahr() == rhs.jahr() &&
            this->monat() == rhs.monat() &&
            this->tag() == rhs.tag()
        );
    }

    inline bool operator!= (Datum& rhs) {
        return !operator== (rhs);
    }

    inline bool operator< (Datum& rhs) {
        if (this->jahr() < rhs.jahr()) {
            return true;
        }
        else if (this->jahr() > rhs.jahr()) {
            return false;
        }
        else { // lhs.jahr() == rhs.jahr()
            if (this->monat() < rhs.monat()) {
                return true;
            }
            else if (this->monat() > rhs.monat()) {
                return false;
            }
            else { // lhs.monat() == rhs.monat()
                if (this->tag() < rhs.tag()) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }
    }

    inline bool operator> (Datum& rhs) {
        return operator< (rhs);
    }

    inline bool operator<= (Datum& rhs) {
        return !operator> (rhs);
    }

    inline bool operator>= (Datum& rhs) {
        return !operator< (rhs);
    }
};

#endif /* DATUM_H_ */
