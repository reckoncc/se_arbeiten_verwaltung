#ifndef ADMINISTRATOR_MANAGEMENT_H
#define ADMINISTRATOR_MANAGEMENT_H

#include <QMainWindow>
#include <QPushButton>

namespace Ui {
class Administrator_Management;
}

class Administrator_Management : public QMainWindow
{
    Q_OBJECT

public:
    explicit Administrator_Management(QWidget *parent = 0);
    ~Administrator_Management();

private:
    Ui::Administrator_Management *ui;
    int table_columns = 3;
    int table_rows = 0;
};

#endif // ADMINISTRATOR_MANAGEMENT_H
