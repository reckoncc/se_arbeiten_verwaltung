#include "login_window.h"
#include "dozent_login.h"
#include "administrator_login.h"
#include "administrator_management.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Administrator_Management w;
    w.show();
    return a.exec();
}
