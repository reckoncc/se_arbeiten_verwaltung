#include "administrator_management.h"
#include "ui_administrator_management.h"

Administrator_Management::Administrator_Management(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Administrator_Management)
{
    ui->setupUi(this);

    //Menü-Leiste einfärben
    ui->Menue_Leiste->setStyleSheet("QMenuBar {color : white;}");
    ui->Programm->setStyleSheet("QMenu {color : white;}");
    ui->Benutzer->setStyleSheet("QMenu {color : white;}");

    //Tabelle einrichten
    ui->Dozenten_Tabelle->setColumnCount(table_columns);
    for (int it = 0; it < table_columns; it++)
    {
        ui->Dozenten_Tabelle->setColumnWidth(it, 263);
    }
    ui->Dozenten_Tabelle->setHorizontalHeaderItem(0, new QTableWidgetItem("Nachname"));
    ui->Dozenten_Tabelle->setHorizontalHeaderItem(1, new QTableWidgetItem("Vorname"));
    ui->Dozenten_Tabelle->setHorizontalHeaderItem(2, new QTableWidgetItem("Email"));
    //TODO: Bei Angabe der Daten und Tabelle hier Tabelleneinträge entgegennehmen und weiterverarbeiten!
    //Dozenten Hinzufügen Button
    ui->Add_Dozent->setText("");
    ui->Add_Dozent->setStyleSheet("QPushButton {image: url(:/Icons/dozent_Neu.png);}");
    ui->Add_Dozent->setFixedHeight(75);
    ui->Add_Dozent->setFixedWidth(75);

    //Dozenten Löschen Button
    ui->Delete_Dozent->setText("");
    ui->Delete_Dozent->setStyleSheet("QPushButton {image: url(:/Icons/dozent_Loeschen.png);}");
    ui->Delete_Dozent->setFixedHeight(75);
    ui->Delete_Dozent->setFixedWidth(75);

    //Suchleistenlabel und Suchleiste selbst anlegen
    ui->Suchleisten_Beschreibung->setText("Einfache Suche. Mögliche Suchen:\nNachname, Vorname, Email");
    ui->Suchleisten_Beschreibung->setStyleSheet("QLabel {color : white;}");
    ui->Suchleisten_Beschreibung->setFont(*(new QFont("Arial", 12, QFont::Bold)));

    ui->Suchleiste->setStyleSheet("QLineEdit {color : white;}");
    ui->SearchButton->setText("");
    ui->SearchButton->setStyleSheet("QPushButton {image: url(:/Icons/magnifying_glasssmall.png);}");
    ui->SearchButton->setFixedHeight(30);
    ui->SearchButton->setFixedWidth(30);

}

Administrator_Management::~Administrator_Management()
{
    delete ui;
}
