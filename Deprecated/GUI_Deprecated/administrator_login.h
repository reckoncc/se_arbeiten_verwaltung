#ifndef ADMINISTRATOR_LOGIN_H
#define ADMINISTRATOR_LOGIN_H

#include <QMainWindow>

namespace Ui {
class Administrator_Login;
}

class Administrator_Login : public QMainWindow
{
    Q_OBJECT

public:
    explicit Administrator_Login(QWidget *parent = 0);
    ~Administrator_Login();

private:
    Ui::Administrator_Login *ui;
};

#endif // ADMINISTRATOR_LOGIN_H
