#include "login_window.h"
#include "ui_login_window.h"

Login_Window::Login_Window(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Login_Window)
{
    ui->setupUi(this);
    int button_sizes = 150;
    QString * fontsheet = new QString("QLabel {color : white;}");
    QFont * labelfont = new QFont("Arial", 12, QFont::Bold);

    //Menubar einstellen
    ui->Menuleiste->setStyleSheet("QMenuBar {color : white;}");
    ui->Programm->setStyleSheet("QMenu {color : white;}");

    //Button-Bilder einstellen
    ui->Admin_Button->setFixedHeight(button_sizes);
    ui->Admin_Button->setFixedWidth(button_sizes);
    ui->Admin_Button->setStyleSheet("QPushButton {image: url(:/Icons/administrator.png);}");
    ui->Admin_Button->setText("");

    ui->Dozent_Button->setFixedHeight(button_sizes);
    ui->Dozent_Button->setFixedWidth(button_sizes);
    ui->Dozent_Button->setStyleSheet("QPushButton {image: url(:/Icons/dozent.png);}");
    ui->Dozent_Button->setText("");

    ui->Gast_Button->setFixedHeight(button_sizes);
    ui->Gast_Button->setFixedWidth(button_sizes);
    ui->Gast_Button->setStyleSheet("QPushButton {image: url(:/Icons/student.png);}");
    ui->Gast_Button->setText("");

    //Button-Labels einstellen
    ui->Admin_Label->setText("Administrator");
    ui->Admin_Label->setStyleSheet(*fontsheet);
    ui->Admin_Label->setFont(*labelfont);
    ui->Admin_Label->setAlignment(Qt::AlignCenter);

    ui->Dozent_Label->setText("Dozent");
    ui->Dozent_Label->setStyleSheet(*fontsheet);
    ui->Dozent_Label->setFont(*labelfont);
    ui->Dozent_Label->setAlignment(Qt::AlignCenter);

    ui->Gast_Label->setText("Gast");
    ui->Gast_Label->setStyleSheet(*fontsheet);
    ui->Gast_Label->setFont(*labelfont);
    ui->Gast_Label->setAlignment(Qt::AlignCenter);

    //Login-Label
    ui->Login_Label->setText("Bitte wählen Sie eine Accountart aus");
    ui->Login_Label->setStyleSheet(*fontsheet);
    ui->Login_Label->setFont(*(new QFont("Arial", 16, QFont::Bold)));
    ui->Login_Label->setAlignment(Qt::AlignCenter);
}

Login_Window::~Login_Window()
{
    delete ui;
}
