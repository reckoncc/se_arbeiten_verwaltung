#include "dozent_login.h"
#include "ui_dozent_login.h"

Dozent_Login::Dozent_Login(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Dozent_Login)
{
    ui->setupUi(this);
    //Standardfont and Standard Fontsheet
    QFont * labelstd = new QFont("Arial", 12, QFont::Bold);
    QString * fontsheet = new QString("QLabel {color : white;}");
    //Menüleiste weiß färben
    ui->MainMenu->setStyleSheet("QMenuBar {color : white;}");
    ui->Programm->setStyleSheet("QMenu {color : white;}");

    //Zurück-Button
    ui->Zurueck_Button->setText("");
    ui->Zurueck_Button->setFixedHeight(100);
    ui->Zurueck_Button->setFixedWidth(100);
    ui->Zurueck_Button->setStyleSheet("QPushButton {image: url(:/Icons/Arrow_Back.png);}");

    //Textlabel festsetzen
    ui->Text_Label->setFont(*(new QFont("Arial", 18, QFont::Bold)));
    ui->Text_Label ->setText("Bitte melden Sie sich an.");
    ui->Text_Label->setAlignment(Qt::AlignCenter);
    ui->Text_Label->setStyleSheet(*fontsheet);

    //Email und Passwortfeld einstellen
    ui->Email_Label->setFont(*labelstd);
    ui->Email_Label->setStyleSheet(*fontsheet);
    ui->Email_Label->setFixedHeight(20);
    ui->Email_Label->setText("Email");

    ui->Email_Eingabe->setFont(*labelstd);
    ui->Email_Eingabe->setStyleSheet("QLineEdit {color : white;}");
    ui->Email_Eingabe->setFixedHeight(25);
    ui->Email_Eingabe->setMaxLength(50);

    ui->Passwort_Label->setFont(*labelstd);
    ui->Passwort_Label->setStyleSheet(*fontsheet);
    ui->Passwort_Label->setFixedHeight(20);
    ui->Passwort_Label->setText("Passwort");

    ui->Passwort_Eingabe->setFont(*labelstd);
    ui->Passwort_Eingabe->setStyleSheet("QLineEdit {color : white;}");
    ui->Passwort_Eingabe->setFixedHeight(22);
    ui->Passwort_Eingabe->setMaxLength(50);
    ui->Passwort_Eingabe->setEchoMode(QLineEdit::Password);

    //Clear und Login-Button
    ui->Clear_Button->setFont(*labelstd);
    ui->Clear_Button->setStyleSheet("QPushButton {color : white;}");
    ui->Clear_Button->setFixedHeight(75);
    ui->Clear_Button->setFixedWidth(125);
    ui->Clear_Button->setText("Leeren");

    ui->Login_Button->setFont(*labelstd);
    ui->Login_Button->setStyleSheet("QPushButton {color : white;}");
    ui->Login_Button->setFixedHeight(75);
    ui->Login_Button->setFixedWidth(125);
    ui->Login_Button->setText("Login");

    //Dozentenbild einfügen
    ui->Dozent_Bild->setText("");
    ui->Dozent_Bild->setStyleSheet("QLabel {image: url(:/Icons/dozent.png);}");
    ui->Dozent_Bild->setFixedHeight(150);
    ui->Dozent_Bild->setFixedWidth(150);

    //Dozentenbild beschriften
    ui->Dozent_Name_Label->setText("Dozent");
    ui->Dozent_Name_Label->setFont(*labelstd);
    ui->Dozent_Name_Label->setStyleSheet(*fontsheet);
    ui->Dozent_Name_Label->setAlignment(Qt::AlignCenter);
}

Dozent_Login::~Dozent_Login()
{
    delete ui;
}
