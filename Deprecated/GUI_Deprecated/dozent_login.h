#ifndef DOZENT_LOGIN_H
#define DOZENT_LOGIN_H

#include <QMainWindow>

namespace Ui {
class Dozent_Login;
}

class Dozent_Login : public QMainWindow
{
    Q_OBJECT

public:
    explicit Dozent_Login(QWidget *parent = 0);
    ~Dozent_Login();
    bool check_Mail(const QString & mail);


private:
    Ui::Dozent_Login *ui;
};

#endif // DOZENT_LOGIN_H
