#ifndef BENUTZER_H
#define BENUTZER_H

/*!
 *\deprecated
 * \brief Die Benutzer Klasse
 * \author Nils Birkner
 * \details Die Klasse Benutzer ist eine abstrakte Klasse, die die Gesamtheit aller
 * Benutzer der Software definiert.
 * Sie besitzt lediglich zwei abstrakte Funktionen und einen boolean, die von anderen Benutzern geerbt
 * werden.
 */
class Benutzer
{
    public:
        Benutzer();
        ~Benutzer();
        /*!
         * \brief Funktion: abmelden
         * \return boolean
         * Die Abstrakte Funktion Abmelden ist für das Abmelden eines Benutzers
         * zuständig. Jeder Benutzer besitzt diese Funktion.
         */
        bool abmelden();
        /*!
         * \brief Funktion: anmelden
         * \return boolean
         * Die Abstrakte Funktion anmelden ist für das Anmelden jedes Benutzers
         * zuständig. Jeder Benutzer besitzt diese Funktion.
         */
        virtual bool anmelden() = 0;

};

#endif // BENUTZER_H
