#include "check_logins.h"

#include <QSqlQuery>
#include <QVariant>
#include <QDebug>

Check_Logins::Check_Logins()
{

}

Check_Logins::~Check_Logins()
{

}

Check_Logins & Check_Logins::get_checklogin()
{
    static Check_Logins instance = Check_Logins();
    return instance;
}

bool Check_Logins::check_admin_login(const QString &mail, const QString &password)
{
    QSqlQuery query;
    QString _mail, _kennwort;
    query.prepare("SELECT * FROM Admin WHERE email_admin = ?");
    query.addBindValue(mail);
    if(query.exec())
    {
        query.first();
        if(!(query.isValid()))
        {
            return false;
        }
        _mail = query.value(0).toString();
        _kennwort = query.value(1).toString();
    }
    else
    {
        qDebug() << "Uncool!";
        return false;
    }
    int check = QString::compare(_mail, mail, Qt::CaseSensitive);
    if(check != 0)
    {
        return false;
    }
    else
    {
        std::hash<std::string>hs;
        std::basic_string<char>bs = password.toStdString();
        QString hashed = QString::number(hs(bs));
        check = QString::compare(_kennwort, hashed, Qt::CaseSensitive);
        if(check != 0)
        {
           return false;
        }
        else
       {
           return true;
       }
    }
}

bool Check_Logins::check_dozent_login(const QString &mail, const QString &password)
{
    QSqlQuery query;
    QString _mail, _kennwort;
    query.prepare("SELECT Email_Dozent, Kennwort FROM Dozent WHERE email_dozent = ?");
    query.addBindValue(mail);
    if(query.exec())
    {
        query.first();
        if(!(query.isValid()))
        {
            return false;
        }
        _mail = query.value(0).toString();
        _kennwort = query.value(1).toString();
    }
    else
    {
        qDebug() << "Uncool!";
        return false;
    }
    int check = QString::compare(_mail, mail, Qt::CaseSensitive);
    if(check != 0)
    {
        return false;
    }
    else
    {
        std::hash<std::string>hs;
        std::basic_string<char>bs = password.toStdString();
        QString hashed = QString::number(hs(bs));
        check = QString::compare(_kennwort, hashed, Qt::CaseSensitive);
        if(check != 0)
        {
           return false;
        }
        else
       {
           return true;
       }
    }
}

bool Check_Logins::check_names(const QString &vorname, const QString &nachname)
{
    if(vorname == "" || nachname == "")
    {
        return false;
    }
    else
    {
        return true;
    }
}

bool Check_Logins::check_mail(const QString &mail)
{
    if(mail == "")
    {
        return false;
    }
    else
    {
        bool at_flag = false;
        for(auto it : mail)
        {
            if(it == "@"){at_flag = true;}
            if(it == ";"){return false;}
        }
        if(at_flag == true)
        {
            return true;
        }
        else{return false;}
    }
}

bool Check_Logins::check_kennwort(const QString &kennwort)
{
    if (kennwort == ""){return false;}
    else{return true;}
}

bool Check_Logins::check_arbeit(const QString &titel,const QString &stichworte, const QList<QString> & bearbeiter)
{
    //Teste ob die eingetragenen titel oder bearbeiter-Werte leer sind
    if(titel == "" || stichworte == "")
    {
        return false;
    }
    if(bearbeiter.empty())
    {
        return false;
    }
    return true;
}
