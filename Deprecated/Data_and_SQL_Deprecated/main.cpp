#include <QCoreApplication>
#include <QDebug>
#include <QtSql>

#include <arbeit.h>
#include<arbeitenContainer.h>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    qDebug()<<"start";
    QSqlDatabase mydb;
    mydb = QSqlDatabase::addDatabase("QSQLITE");
    mydb.setDatabaseName("C:/Vorlesungen/SE/SE_QT/mydb.sqlite");

    if (! mydb.open()) {
        qDebug()<<"Problem by opening the database.";
    }

    QString q1 = "CREATE TABLE Admin ("
                 "Email_Admin varchar(50) primary key not null,"
                 "Kennwort varchar(50) not null);";

    QString q2 = "CREATE TABLE Dozent ("
                "Email_Dozent varchar(50) primary key not null,"
                "Vorname varchar(50) not null,"
                "Nachname varchar(50) not null,"
                "Kennwort varchar(50) not null);";

    QString q3 = "CREATE TABLE Arbeit ("
                                        "Arbeit_id integer PRIMARY KEY AUTOINCREMENT,"
                                        "Studiengang varchar(50) not null,"
                                        "Titel varchar(100) not null,"
                                        "Art varchar(50) not null,"
                                        "Status varchar(50) not null,"
                                        "Stichwortliste varchar(200) not null,"
                                        "Email_Dozent varchar(50),"
                                        "Erlaeuterung varchar(100),"
                                        "foreign key (Email_Dozent) references Dozent);";

    QString q4 = "CREATE TABLE Abschlussarbeit ("
                    "Arbeit_id int primary key references Arbeit not null,"
                    "Firma varchar(50),"
                    " bis date not null,"
                    " von date not null );";

    QString q5 = "CREATE TABLE Projektarbeit ("
                " Arbeit_id int primary key references Arbeit not null,"
                " Semester varchar(20) not null,"
                " Schwerpunkt varchar(10) not null);";

    QString q6 = "CREATE TABLE bearbeitung("
                "Benutzer_name varchar(50),"
                "Arbeit_id int,"
                "foreign key (Arbeit_id) references Arbeit,"
                "primary key(Benutzer_name,Arbeit_id));";


      QList<QString> list;
      list.append(q1);
      list.append(q2);
      list.append(q3);
      list.append(q4);
      list.append(q5);
      list.append(q6);

  /* QString s;
   foreach (s , list) {
        QSqlQuery query;
        if (! query.exec(s)) {
            qDebug()<<"Error by creating table.";
            qDebug()<< s;
        }
    }*/

    QList<QString>  bearbeiter, bearbeiter2;
    bearbeiter.append("Marina");
    bearbeiter.append("Ariel");
    bearbeiter2.append("Marina");
    bearbeiter2.append("Ariel");
    bearbeiter2.append("Michael");
    Dozent d = *new Dozent("heinlein@hs-aalen.de","Heinlein","Christian");
    Dozent d2 = *new Dozent("karg@hs-aalen.de","Karg","Christoph");


    Arbeit arbeit = *new Arbeit("Projekt","TEST","KI; Master","abgeschlossen","SE","NICHT",bearbeiter,&d);
    Arbeit arbeit2 = *new Arbeit("MA","Master"," Master","in Bearbeitung","IN","NICHT",bearbeiter2,&d);
    Arbeit arbeit3 = *new Arbeit("MA","Bachelor"," Master","in Bearbeitung","MI","NICHT",bearbeiter,&d);
    Arbeit arbeit4 = *new Arbeit("MA","Bachelor"," Master","in Bearbeitung","AI","NICHT",bearbeiter,&d2);

    //ArbeitenContainer::instance()->insert(arbeit);
    //ArbeitenContainer::instance()->insert(arbeit2);
    //ArbeitenContainer::instance()->insert(&arbeit3);
    //ArbeitenContainer::instance()->insert(&arbeit4);
    qDebug()<<arbeit.arbeit_id();
    ArbeitenContainer::instance()->remove(arbeit);

    qDebug()<<"end";

    return a.exec();
}

