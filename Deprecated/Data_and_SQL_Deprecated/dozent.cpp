#include "dozent.h"
#include "ArbeitenContainer.h"
#include "Arbeit.h"
#include "check_logins.h"

#include <utility>

Dozent::Dozent(const QString & vorname, const QString & nachname, const QString & mail, const QString & kennwort):
    _vorname(vorname), _nachname(nachname), _mail(mail), _kennwort(kennwort)
{
}

Dozent::Dozent(QString &&vorname, QString &&nachname, QString &&mail, QString &&kennwort):
    _vorname(std::move(vorname)), _nachname(std::move(nachname)), _mail(std::move(mail)), _kennwort(std::move(kennwort))
{
}

Dozent::~Dozent()
{
}

bool Dozent::arbeit_eintragen(const QString & schwerpunkt, const QString &titel, const QString &art, Dozent &betreuer,
                                              const QString &status, const QString &erlaeuterung, const QString &stichworte, const QList<QString> & bearbeiter)
{
    std::unique_ptr<ArbeitenContainer>_ac(ArbeitenContainer::instance());
    Check_Logins _cl = Check_Logins::get_checklogin();
    if(!(_cl.check_arbeit(titel, stichworte, bearbeiter)))
    {
        return false;
    }
    std::unique_ptr<Dozent>_doz(new Dozent(betreuer.vorname(), betreuer.nachname(), betreuer.mail(), betreuer.kennwort()));
    std::unique_ptr<Arbeit> _arbeit(new Arbeit(art, titel, stichworte, status, schwerpunkt, erlaeuterung, bearbeiter, _doz.get()));
    _ac->insert(_arbeit.get());
    _doz.reset(nullptr);
    _arbeit.reset(nullptr);
    _ac.reset(nullptr);
    return true;

}
