#include "Datum.h"

Datum::Datum(const int jahr, const int monat, const int tag)
  : _jahr(jahr), _monat(monat), _tag(tag)
{

}

int Datum::jahr() { return _jahr; }
int Datum::monat() { return _monat; }
int Datum::tag() { return _tag; }

void Datum::set_jahr(const int jahr) { this->_jahr = jahr; }
void Datum::set_monat(const int monat) { this->_monat = monat; }
void Datum::set_tag(const int tag) { this->_tag = tag; }
