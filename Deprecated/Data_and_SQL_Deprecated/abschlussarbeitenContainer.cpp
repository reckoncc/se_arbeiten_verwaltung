#include <abschlussarbeitencontainer.h>
#include <arbeitenContainer.h>
#include <QtSql>

AbschlussarbeitenContainer* AbschlussarbeitenContainer::instance_abschlussarbeiten(){
    static AbschlussarbeitenContainer* _instance;
    if (_instance == 0){
        _instance = new AbschlussarbeitenContainer();
    }
    return _instance;
}

bool AbschlussarbeitenContainer::insertAbschlussarbeit(Abschlussarbeit *ab){
        Arbeit arbeit = *new Arbeit(ab->art(),ab->titel(),ab->stichwortliste(),ab->status(),ab->studiengang(),
                                   ab->erlaeuterung(),ab->bearbeiter(),ab->betreuer());

        int arbeit_id = ArbeitenContainer::instance()->insert(&arbeit);
       if ( arbeit_id != -1){

           QSqlQuery query;

           query.prepare("INSERT INTO Abschlussarbeit (arbeit_id,firma,bis,von)"
                         "VALUES (:id,:f,:b,:v)");
           query.bindValue(":id",arbeit_id);
           query.bindValue(":f",ab->firma());
           query.bindValue(":b",ab->bis()->toString("YYYY.MM.DD"));
           query.bindValue(":v",ab->von()->toString("YYYY.MM.DD"));

           if (query.exec()){
               qDebug()<<"cool by inserting in Abschlussarbeit!";
               return true;
           }
       }
       else{
           qDebug()<<"Uncool while inserting in Abschlussarbeit!";
           return false;
       }

}

bool AbschlussarbeitenContainer:: updateAbschlussarbeit(int id, Abschlussarbeit * ab) {

    Arbeit arbeit = *new Arbeit(ab->art(),ab->titel(),ab->stichwortliste(),ab->status(),ab->studiengang(),
                               ab->erlaeuterung(),ab->bearbeiter(),ab->betreuer());
    bool answer = ArbeitenContainer::instance()->update(id,&arbeit);
    if (answer){
        QSqlQuery query;
        query.prepare("UPDATE Abschlussarbeit Set firma=:f, bis=:b, von=:v "
                      "WHERE arbeit_id =:id");
        query.bindValue(":f",ab->firma());
        query.bindValue(":b",ab->bis()->toString("YYYY.MM.DD"));
        query.bindValue(":v",ab->von()->toString("YYYY.MM.DD"));
        query.bindValue(":id",id);

        if (query.exec()){
            qDebug()<<"Cool update in Abschlussarbeit";
            return true;
        }
        else {
            qDebug()<<"Uncool update in Abschlussarbeit";
            return false;
        }
    }
    else{

        qDebug()<<"Uncool update in Abschlussarbeit";
        return false;
    }


}

bool AbschlussarbeitenContainer:: removeAbschlussarbeit(int id){
    QSqlQuery query;
    query.prepare("Delete From Abschlussarbeit where arbeit_id = ?");
    query.addBindValue(id);
    if (query.exec()){
        qDebug()<<"Cool delete in Abschlussarbeit";
        return ArbeitenContainer::instance()->remove(id);

    }
    else{
        return false;
    }
}
bool AbschlussarbeitenContainer::searchAbschlussarbeit(int id){
    QSqlQuery query;
    query.prepare("Select * from Abschlussarbeit where arbeit_id = ?");
    query.addBindValue(id);
    query.exec();
    if (query.next()){
        qDebug()<<"Abschlussarbeit in der DB vorhanden!";
        return true;
    }
    else {
        qDebug()<<"Abschlussarbeit nicht in der DB.";
        return false;
    }
}
