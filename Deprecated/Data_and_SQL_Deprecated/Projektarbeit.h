#ifndef PROJEKTARBEIT
#define PROJEKTARBEIT
#include <arbeit.h>

class Projektarbeit: public Arbeit {
public:
    Projektarbeit(string art, string titel, vector<string> stwort,string stat, string stg, string erl, vector<string> bearbeiter, string punkt,string s)
                : Arbeit(art, titel, stwort, stat, stg, erl, bearbeiter)
                 {
                    this->_schwerpunkt = punkt;
                    this->_semester = s;
                 }

    string schwerpunkt();
    void set_schwerpunkt(string s);

    string semester();
    void set_semester(string se);



private:
    string _schwerpunkt;
    string _semester;
};

#endif // PROJEKTARBEIT

