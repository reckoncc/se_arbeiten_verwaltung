#ifndef ABSCHLUSSARBEIT
#define ABSCHLUSSARBEIT
#include <arbeit.h>;
#include "Datum.h";

class Abschlussarbeit: public Arbeit {

public:
    Abschlussarbeit(string art, string titel, vector<string> stwort, string stat, string stg, string erl, vector<string> bearbeiter, string f, Datum v, Datum b)
      : Arbeit(art, titel, stwort, stat, stg, erl, bearbeiter)
    {
        this->_firma = f;
        this->_von  = v;
        this->_bis = b;
    }

    string firma();
    void set_firma(string f);

    Datum von();
    void set_von(Datum v);

    Datum bis();
    void set_bis(Datum b);



private:
    string _firma;
    Datum _von;
    Datum _bis;
};

#endif // ABSCHLUSSARBEIT


