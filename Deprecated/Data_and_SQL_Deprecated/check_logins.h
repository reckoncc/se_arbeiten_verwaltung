#ifndef CHECK_LOGINS_H
#define CHECK_LOGINS_H

#include<QString>
#include <QList>

#include<memory>

class Check_Logins;
class Dozent;

class Check_Logins
{
public:
    ~Check_Logins();
    static Check_Logins & get_checklogin();
    bool check_admin_login(const QString & mail, const QString & password);
    bool check_dozent_login(const QString & mail, const QString & password);
    bool check_names(const QString &vorname, const QString &nachname);
    bool check_mail(const QString &mail);
    bool check_kennwort(const QString &kennwort);
    bool check_arbeit(const QString &titel,const QString &stichworte, const QList<QString> & bearbeiter);
private:
    Check_Logins();
};

#endif // CHECK_LOGINS_H
