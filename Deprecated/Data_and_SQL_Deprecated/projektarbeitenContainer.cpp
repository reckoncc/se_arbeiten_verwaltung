#include<projektarbeitencontainer.h>
#include<arbeitenContainer.h>
#include<QtSql>

ProjektarbeitenContainer* ProjektarbeitenContainer::instance_projektarbeiten(){
    static ProjektarbeitenContainer* _instance;
    if (_instance == 0){
        _instance = new ProjektarbeitenContainer();
    }
    return _instance;
}

bool ProjektarbeitenContainer::insertProjektarbeit(Projektarbeit *pa){
        Arbeit arbeit = *new Arbeit(pa->art(),pa->titel(),pa->stichwortliste(),pa->status(),pa->studiengang(),
                                   pa->erlaeuterung(),pa->bearbeiter(),pa->betreuer());

        int arbeit_id = ArbeitenContainer::instance()->insert(&arbeit);
        if ( arbeit_id != -1){

           QSqlQuery query;

           query.prepare("INSERT INTO Projektarbeit (arbeit_id,semester,schwerpunkt)"
                         "VALUES (:id,:se,:spkt)");
           query.bindValue(":id",arbeit_id);
           query.bindValue(":se",pa->semester());
           query.bindValue(":spkt",pa->schwerpunkt());

           if (query.exec()){
               qDebug()<<"cool by inserting in Projektarbeit!";
               return true;
           }
       }
       else{
           qDebug()<<"Uncool while inserting in Projektarbeit!";
           return false;
       }

}

bool ProjektarbeitenContainer:: updateProjektarbeit(int id, Projektarbeit * pa) {

    Arbeit arbeit = *new Arbeit(pa->art(),pa->titel(),pa->stichwortliste(),pa->status(),pa->studiengang(),
                               pa->erlaeuterung(),pa->bearbeiter(),pa->betreuer());
    bool answer = ArbeitenContainer::instance()->update(id,&arbeit);
    if (answer){
        QSqlQuery query;
        query.prepare("UPDATE Projektarbeit Set semester=:se, schwerpunkt =:spkt "
                      "WHERE arbeit_id =:id");
        query.bindValue(":se",pa->semester());
        query.bindValue(":spkt",pa->schwerpunkt());
        query.bindValue(":id",id);

        if (query.exec()){
            qDebug()<<"Cool update in Projektarbeit";
            return true;
        }
        else {
            qDebug()<<"Uncool update in Projektarbeit";
            return false;
        }
    }
    else{

        qDebug()<<"Uncool update in Projektarbeit";
        return false;
    }


}

bool ProjektarbeitenContainer:: removeProjektarbeit(int id){
    QSqlQuery query;
    query.prepare("Delete From Projektarbeit where arbeit_id = ?");
    query.addBindValue(id);
    if (query.exec()){
        qDebug()<<"Cool delete in Projektarbeit";
        return ArbeitenContainer::instance()->remove(id);

    }
    else{
        return false;
    }
}
bool ProjektarbeitenContainer::searchProjektarbeit(int id){
    QSqlQuery query;
    query.prepare("Select * from Projektarbeit where arbeit_id = ?");
    query.addBindValue(id);
    query.exec();
    if (query.next()){
        qDebug()<<"Projektarbeit in der DB vorhanden!";
        return true;
    }
    else {
        qDebug()<<"Projektarbeit nicht in der DB.";
        return false;
    }
}
