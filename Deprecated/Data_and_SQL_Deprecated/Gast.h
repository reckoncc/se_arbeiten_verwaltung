#ifndef GAST_H
#define GAST_H

#include<QList>
#include<memory>
#include <time.h>
#include<QString>

class Arbeit;
class Dozent;
/*!
 * \brief Die Gast Klasse
 * \author Nils Birkner
 * Die Gast Klasse repräsentiert einen Studenten der Hochschule Aalen.
 * Dieser kann ohne einen Email-Login sich direkt anmelden und nach verschiedenen
 * Arbeiten suchen. Weitere Funktionen besitzt er nicht.
 */
class Gast
{
    //Damit vorweg Arbeit bekannt ist.
public:
    /*!
     * \brief Gast
     * Der Konstruktor Gast ist leer und ruft lediglich den Benutzer-Konstruktor
     * _active auf.
     */
    Gast();

    /*!
      \brief ~Gast
     * Der Destruktor von Gast ist leer, da es keine speziellen Objekte zu löschen gibt.
     */
    ~Gast();
    /*!
     * \brief Funktion: suche_zeitraum
     * \param von
     * \param bis
     * \return Unique_Pointer auf eine Liste von Arbeiten
     * Die Funktion suche_zeitraum sucht alle Arbeiten aus, die in dem angegebenen Zeitintervall
     * angegeben wurden.
     */
    std::unique_ptr<QList<Arbeit>> suche_zeitraum(time_t von, time_t bis);
    /*!
     * \brief Funktion: suche_art_arbeit
     * \param arbeit
     * \return Unique_Pointer auf eine Liste von Arbeiten
     * Die Funktion suche_art_arbeit sucht alle Arbeiten aus,
     * die die übergebene Art der Arbeit beinhalten.
     */
    std::unique_ptr<QList<Arbeit>> suche_art_arbeit(Arbeit * arbeit);
    /*!
     * \brief suche_bearbeiter_name
     * \param bearbeiter
     * \return Unique_Pointer auf eine Liste von Arbeiten
     * Die Funktion suche_bearbeiter_name sucht alle Arbeiten aus, deren
     * Inhalt den Namen des übergebenen Bearbeiters beinhalten.
     */
    std::unique_ptr<QList<Arbeit>> suche_bearbeiter_name(QString bearbeiter);
    /*!
     * \brief suche_betreuer_name
     * \param dozent
     * \return Unique_Pointer auf eine Liste von Arbeiten
     * Die Funktion suche_betreuer_name sucht alle Arbeiten aus, deren Inhalt
     * den übergebenen Dozenten beinhalten.
     */
    std::unique_ptr<QList<Arbeit>> suche_betreuer_name(Dozent * dozent);

    //Mit Marina Besprochen, Schwerpunkt auf QString zu setzen, nicht enumclass!
    /*!
     * \brief suche_schwerpunkt
     * \param schwerpunkt
     * \return Unique_Pointer auf eine Liste von Arbeiten
     * Die Funktion suche_schwerpunkt sucht alle Arbeiten aus, deren Inhalt
     * den übergebenen Schwerpunkt enthalten.
     */
    std::unique_ptr<QList<Arbeit>> suche_schwerpunkt(QString schwerpunkt);
    /*!
     * \brief suche_titel
     * \param titel
     * \return Unique_Pointer auf eine Liste von Arbeiten
     * Die Funktion suche_titel sucht alle Arbeiten aus, deren Inhalt
     * den übergebenen Titel beinhalten.
     */
    std::unique_ptr<QList<Arbeit>> suche_titel(QString titel);
    /*!
     * \brief suche_stichwort
     * \param stichwortliste
     * \return Unique_Pointer auf eine Liste von Arbeiten
     * Die Funktion suche_stichwort sucht alle Arbeiten aus, deren Inhalt
     * die eines- oder mehrere Begriffe der übergebenen Stichwortliste beinhalten.
     */
    std::unique_ptr<QList<Arbeit>> suche_stichwort(QList<QString> stichwortliste);
    bool anmelden(){return true;}
    bool abmelden(){return true;}
};

#endif // GAST_H
