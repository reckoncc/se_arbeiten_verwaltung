/*!
 * \author Marina MENE TEDAYEM
 */
#include "Dozentencontainer.h"
#include <QtSql>
#include <utility>

/*!
 * \brief DozentenContainer::dozentenContainer
 * \return DozentenContainer *
 * \details Da der Konstruktor von der Klasse DozentenContainer privat ist, braucht man eine öffentliche
 * Methode, um auf die einzige Instanz dieser Klasse zugreifen zu können.
 * Wenn noch kein Objekt der Klasse DozentenContainer existiert, wird ein neues erzeugt.
 * Am Ende wird es zurückgegeben.
 */

DozentenContainer* DozentenContainer:: dozentenContainer(){
    static DozentenContainer* container;
    if (!container){
        container = new DozentenContainer();
    }
    return container;
}

/*!
 * \brief DozentenContainer::get_list_dozenten
 * \return QList <Dozent *>
 * \details sie gibt die Liste aller Dozenten zurück, die in der Datenbank sind.
 */
QList<Dozent *> DozentenContainer::get_list_dozenten(){
    return list_dozenten;
}

/*!
 * \brief DozentenContainer::insert
 * \param vorname
 * \param nachname
 * \param mail
 * \param hashedPsw
 * \return bool
 * \details Diese Methode fügt einen neuen Dozent in der Datenbank ein.
 * Dafür werden die Parameter, die übergeben sind, in den Spalten der Tabelle Dozent eingetragen.
 */
bool DozentenContainer::insert(QString vorname, QString nachname, QString mail, QString hashedPsw){


    Dozent *neu_dozent = new Dozent (vorname,nachname,mail,hashedPsw);
    DozentenContainer::dozentenContainer()->get_list_dozenten().append(neu_dozent);

    QSqlQuery query;
    query.prepare("INSERT INTO Dozent(email_dozent,vorname,nachname,kennwort)"
                  "VALUES(:mail,:vor,:nach,:kwort)");
    query.bindValue(":mail",mail);
    query.bindValue(":vor",vorname);
    query.bindValue(":nach",nachname);
    query.bindValue(":kwort",hashedPsw);

    if (query.exec()){
        qDebug()<<"Hey new Dozent!!";

        return true;

    }
    else{
        qDebug()<<"Uncool!!";
        return false;
    }
}

/*!
 * \brief DozentenContainer::remove
 * \param mail
 * \return bool
 * \details Diese Methode löscht in der Datenbank den Dozent, dessen Email als Parameter übergeben
 * worden ist. Wenn ein Dozent von der Datenbank gelöscht wird, werden alle Arbeiten , die er
 * betreuet, gelöscht.
 */
bool DozentenContainer:: remove (QString mail){
    QSqlQuery query,query1;

    query.prepare("Delete from Dozent where email_dozent = ?");
    query.addBindValue(mail);

    if (query.exec()){
        qDebug()<<"Cool by removing!";
        query1.prepare("Delete from Arbeit where email_dozent = ?");
        query1.addBindValue(mail);

        if (query1.exec()){
             return true;
        }
        else{
            return false;
        }

    }
    else {
        qDebug()<<"Uncool by removing";
        return false;
    }
}

/*!
 * \brief DozentenContainer::update
 * \param mail
 * \param neu_mail
 * \param neu_nachname
 * \param neu_vorname
 * \param neu_kennwort
 * \return bool
 * \details Diese Methode gilt zur Aktualisierung der Daten eines Dozenten.
 * Sie erhält als Parameter die email vom Dozenten, dere Daten aktualisiert werden sollen und
 * dazu werden noch die neuen Daten übergeben.
 */
bool DozentenContainer::update(QString mail,QString neu_mail,QString neu_nachname,QString neu_vorname,QString neu_kennwort = ""){
    QSqlQuery query;
    if(neu_kennwort != "")
    {
        query.prepare("UPDATE Dozent SET email_dozent=:mail,vorname=:vor, nachname=:nach,kennwort=:kwort "
                      "WHERE email_dozent = :alt_mail");
        query.bindValue(":mail",neu_mail);
        query.bindValue(":vor",neu_vorname);
        query.bindValue(":nach",neu_nachname);
        query.bindValue(":kwort",neu_kennwort);
        query.bindValue(":alt_mail",mail);
    }
    else
    {
        query.prepare("UPDATE Dozent SET email_dozent=:mail,vorname=:vor, nachname=:nach "
                      "WHERE email_dozent = :alt_mail");
        query.bindValue(":mail",neu_mail);
        query.bindValue(":vor",neu_vorname);
        query.bindValue(":nach",neu_nachname);
        query.bindValue(":alt_mail",mail);
    }
    if (query.exec())
    {
        qDebug()<<"Cool by update";
        return true;
    }
    else{
        qDebug()<<"Uncool by update";
        return false;
    }
}
/*!
 * \brief DozentenContainer::getDozent
 * \param mail
 * \return Dozent *
 * \details
 */
Dozent * DozentenContainer::getDozent(QString mail){
    QSqlQuery query;
    QString vorname,nachname,kennwort;
    query.prepare("Select * From Dozent where email_dozent = ?");
    query.addBindValue(mail);

    if (query.exec()){
        while (query.next()){
             vorname = query.value(1).toString();
             nachname = query.value(2).toString();
             kennwort = query.value(3).toString();
        }
        return new Dozent(vorname,nachname,mail,kennwort);
    }
    else{
        return NULL;
    }
}
