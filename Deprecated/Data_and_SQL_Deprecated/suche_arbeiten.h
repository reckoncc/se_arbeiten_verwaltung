#ifndef SUCHE_ARBEITEN_H
#define SUCHE_ARBEITEN_H

//QT-Includes
#include <QList>
#include <QString>
//std:: Includes
#include <memory>
//Vorwegdefinition der Arbeit-Klasse
class Arbeit;

class Suche_Arbeiten
{
public:
    ~Suche_Arbeiten();
    //Da Singleton nur ein Objekt das eine Referenz auf sein Singleton-Objekt zurückgibt
    Suche_Arbeiten& get_arbeitssuche();
    /*Anmerkung: Wenn möglich sollten alle Suchen eine Liste an Arbeiten zurückgeben(Nicht durch die Referenz verwirren lassen)!
     * Wenn das Listenobjekt jedoch zerstört wird am Ende der Funktion, einfach das Objekt selbst zurückgeben und das & beim
     * Rückgabewert einfach löschen!
    */
    QList<Arbeit>& suche_titel(const QString & titel);
    QList<Arbeit>& suche_schwerpunkt(const QString & schwerpunkt);
    QList<Arbeit>& suche_art(const QString & art);
    QList<Arbeit>& suche_betreuer(const QString & betreuername);
    QList<Arbeit>& suche_status(const QString & status);
    //Besonderheit: Da mehrere Stichworte mit Whitespace-getrennt übergeben werden per Liste (wird von mir dann implementiert!), wird hier
    //eine Liste an Stichworten übergeben!
    QList<Arbeit>& suche_stichworte(QList<QString> & liste);
    QList<Arbeit>& suche_bearbeiter(const QString & bearbeiter);

    //Debug-Function
    void print_results(QList<Arbeit> & arbeiten);
private:
    Suche_Arbeiten();
};

#endif // SUCHE_ARBEITEN_H
