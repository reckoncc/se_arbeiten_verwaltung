#ifndef ADMINISTRATOR_H
#define ADMINISTRATOR_H
#include<QString>
#include<tuple>
#include<memory>

//Klasse zum bearbeiten bekannt machen.
class DozentenContainer;
class Dozent_Anlegen;
class Dozent;


/*!
 * \brief Die Administratoren Klasse
 * \author Nils Birkner
 * Diese Klasse stellt den Administrator dar, der für die Verwaltung der Dozenten zuständig ist.
 * Er besitzt den boolean _active von der Basisklasse Benutzer, sowie die QStrings Email und Kennwort
 * und den Integer _id. Er kann Dozenten anlegen mittels der Funktion dozent_anlegen.
 * Er kann Dozenten löschen mittels dozent_loeschen und ein Kennwort generieren mittels
 * kennwort_generieren.
 */

class Administrator
{
private:
    /*!
     * \brief Konstruktor: Administrator
     * \param email
     * \param id
     * \param kennwort
     * Der Administratoren-Kopierkonstruktor nimmt die Email und den Kennworthash des
     * Administrators an und speichert diesen ab.
     */
    Administrator(const QString & email, const QString & kennwort);
    /*!
     * \brief _email
     * Der QString _email beinhaltet die Email des zuständigen Administrators.
     */
    QString _email;
    /*!
     * \brief _kennwort
     * Der QString _kennwort speichert das gehashte Passwort des Administrators.
     */
    QString _kennwort;

    std::shared_ptr<Administrator>_admin;

public:
    /*!
     * \brief Administrator
     * Ein Administratoren-Objekt anzulegen ohne Angabe von Daten ist verboten und wird damit unterbunden.
     */
    Administrator() = delete;
    /*!
      * Ein leerer Destruktor, da nichts speziell in diesem Objekt gelöscht werden muss.
      */
    ~Administrator();
    /*!
     * \brief Funktion: create_administrator
     * \param email
     * \param kennwort
     * \return Administrator-Pointer
     * Die Funktion create_administrator erstellt ein neues Administratoren-Objekt, wenn noch keines existiert.
     * Sollte jedoch eines bereits vorhanden sein, wird dieses stattdessen zurückgegeben (Singleton-Pattern)
     */
    void create_administrator(const QString & email, const QString & kennwort);
    /*!
      * \brief dozent_anlegen
      * \param vorname
      * \param nachname
      * \param email
      * \param kennwort
      * \return boolean
      * Die Funktion dozent_anlegen legt in der Datenbank einen neuen Dozenten an,
      * nachdem alle übergebenen Parameter verifiziert wurden.
      */
     static bool dozent_anlegen(const QString &vorname, const QString &nachname,
                                               const QString &mail, const QString &kennwort);
     /*!
      * \brief dozent_loeschen
      * \param zuloeschen
      * \return boolean
      * Die Funktion dozent_loeschen löscht nach einer Prüfung auf offene Arbeiten den
      * angegebenen Dozenten.
      */
     static bool dozent_loeschen(const QString & mail);
     /*!
      * \brief dozent_updaten
      * \param oldmail
      * \param vorname
      * \param nachname
      * \param mail
      * \param kennwort
      * \return boolean
      * Die Funktion dozent_updaten löscht zuerst anhand der alten Mail
      */
     static bool dozent_updaten(const QString & oldmail, const QString & vorname, const QString & nachname,
                                               const QString & mail, const QString & kennwort);
     /*!
      * \brief kennwort_generieren
      * \return std::tuple
      * Die Funktion kennwort_generieren erstellt ein neues, zufälliges Passwort, das dem Dozenten
      * zugesendet werden kann. Es wird aus diesem außerdem ein Hash erstellt, der in der Datenbank
      * zur Abgleichung beim nächsten Login verwendet werden kann.
      */
    static std::tuple<QString, size_t> kennwort_generieren();
    static Dozent * get_dozent(const QString & mail);

};

#endif // ADMINISTRATOR_H
